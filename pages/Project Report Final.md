- The project developed a easy to use device which can accelerate the diagnosis of
  schizophrenia. However due to lack of the number of electrodes in the custom EEG headset,
  we had to rethink the proposed model and consider only the required number of electrodes
  as needed. We also needed to remove the Fast Fourier Transform component from the pre-
  processing stage becasue it does not create much impact for a two electrode system.
- Exaplin each model. how many layer, accuracy
- Models
	- In our implementation, the 1D CNN which has five one dimensional convolutional layers
	  of kernel size 3 and 5 filters, activated using a sigmoid function had a training accuracy of 83%
	  and a testing accuracy of 67%.
	- Also the one 1D convolutional layer with kernel size 3 and 32 filters, activated using a
	  sigmoid function had a training accuracy of 67% and a testing accuracy of 69%.
	- The CNN-LSTM model four convolutional layers with kernel size 3 and filter count 5,
	  activating using a sigmoid function produced a training accuracy og 87.9% and a validation
	  accuracy of 73.89%.
	- The FFT-CNN-LSTM model two convolutional layers with kernel size 2 and filter count
	  64, activating using a sigmoid function produced a training accuracy og 94% and a validation
	  accuracy of 75.7%.
	- The Two Electrode CNN-LSTM model four convolutional layers with kernel size 3 and
	  filter count 5, activating using a sigmoid function produced a training accuracy og 77.8% and a
	  validation accuracy of 73.7%
- We experimented with five models. The 1D CNN model with 5 one dimensional convolution layers, kernel size 3, 5 filters activated using sigmoid function returned a training accuracy of 83% and a testing accuracy of 67%. Alternatively, the 1D CNN model with 1 convolutional layer with kernel size 3 and 32 filters activated using sigmoid function returned a training accuracy of 67% and a testing accuracy of 69%. The CNN-LSTM model with 4 convolutional layers, kernel size 3, filter count 5 & activated with a sigmoid function produced a training accuracy of 87.9% and validation accuracy of 73%. The FFT-CNN-LSTM model with 2 convolutional layers, kernel size 2, filter count 64 & activated with sigmoid function produced a training accuracy of 94% and a validation accuracy of 75.7%. Finally, the two electrode CNN-LSTM model with 4 convolutional layers, kernel size 3, filter count 5 & activated with sigmoid function returned a training accuracy of 77.8% and a validation accuracy of 73.7%. It is observed that the FFT-CNN-LSTM model had the highest validation accuracy followed by the CNN-LSTM model.
- Draft 1
	- The project developed a easy to use device which can accelerate the diagnosis of
	  schizophrenia. However due to lack of the number of electrodes in the custom EEG headset,
	  we had to rethink the proposed model and consider only the required number of electrodes
	  as needed. We also needed to remove the Fast Fourier Transform component from the pre-
	  processing stage because it does not create much impact for a two electrode system. We experimented with five models. The 1D CNN model with 5 one dimensional convolution layers, kernel size 3, 5 filters activated using sigmoid function returned a training accuracy of 83% and a testing accuracy of 67%. Alternatively, the 1D CNN model with 1 convolutional layer with kernel size 3 and 32 filters activated using sigmoid function returned a training accuracy of 67% and a testing accuracy of 69%. The CNN-LSTM model with 4 convolutional layers, kernel size 3, filter count 5 & activated with a sigmoid function produced a training accuracy of 87.9% and validation accuracy of 73%. The FFT-CNN-LSTM model with 2 convolutional layers, kernel size 2, filter count 64 & activated with sigmoid function produced a training accuracy of 94% and a validation accuracy of 75.7%. Finally, the two electrode CNN-LSTM model with 4 convolutional layers, kernel size 3, filter count 5 & activated with sigmoid function returned a training accuracy of 77.8% and a validation accuracy of 73.7%. It is observed that the FFT-CNN-LSTM model had the highest validation accuracy followed by the CNN-LSTM model.