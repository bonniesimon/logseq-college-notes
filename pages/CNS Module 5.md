public:: true

- ![Module 5.1 by Miss.pdf](../assets/Module_5.1_by_Miss_1642484098931_0.pdf)
- ![Module 5.2 by Miss.pdf](../assets/Module_5.2_by_Miss_1642484105215_0.pdf)
- ![Module 5 Ktustudents.in.pdf](../assets/Module_5_Ktustudents.in_1642662913304_0.pdf)
- Syllabus
	- Network security: Electronic Mail Security: Pretty good privacy- S/MIME. IP Security: Architecture- authentication Header- Encapsulating Security payload- Combining Security associations- Key management.
- # Electronic mail security
	- Two schemes
		- Pretty Good Privacy(PGP)
		- Secure/Multipurpose Internet Mail Extension(S/MIME)
	- Why study email security
		- After web browsing, email is the most widely used network reliant application
		- Mail servers, after web servers, are the most often attacked internet hosts
		- Basic email offers little security
	- Threats to email
		- Confidentiality related threats
			- Could result in unauthorized disclosure of sensitive information
			- Emails are sent in a clear over open networks
			- Emails are stored on potentially insecure clients and mail servers
		- Integrity related threats
			- Could result in unauthorized modification of the email content
			- Somebody could alter the mail in transit or on mail server
		- Authenticity related threats
			- Could result in unauthorized access to an enterprise's email system
		- Availability related threats
			- Could prevent end users from being able to send or receive mails
	- Email security options
		- Secure the server to client connections
			- https access to webmail
			- protection against insecure wireless access
		- Secure the end to end email delivery
			- PGP
- # Pretty Good Privacy (PGP)
	- Q
		- what is PGP
		- does it have anything to do with privacy
		- how does it achieve email security
	- PGP is an email security program or an email security protocol
		- based on the IDEA algorithm for the encryption of plaintext
		- and uses RSA public key algorithm for the encryption of the private key
	- It provides
		- authentication
		- confidentiality
		- compression
		- email compatibility
		- segmentation & reassembly
	- ### Authentication
		- Sender creates message
		- Use SHA-1 to generate a 160 bit hash of the message
		- Uses Private key of sender to encrypt it using RSA and attaches it to the message
		- Receiver uses RSA with public key of sender to decrypt and recover the hash code
		- The message received by the receiver is hashed and compared to the received hash.
		- ![image.png](../assets/image_1642492425523_0.png)
	- ### Confidentiality
		- Sender generate the message and 128 bit random number as session key
		- Encrypts message using CAST-128/IDEA/3DES with session key
		- Encrypt session key with RSA using recipients public key and attach it to the message
		- Receiver uses RSA and private key of the receiver to decrypt the session key
		- Using the session key we will decrypt the message
		- ![image.png](../assets/image_1642492912592_0.png)
	- ### Confidentiality and Authentication
		- ![image.png](../assets/image_1642492947015_0.png)
	- ## Steps taken by PGP at sender site
collapsed:: true
		- The email message is hashed using a hashing function to create a digest
		- The digest is then encrypted to form a signed digest by using sender's private key. Then signed digest is added to the original email message
		- The original message and signed digest are then encrypted using a one time secret key created by the sender
		- This secret key is then encrypted using the receivers public key (This is done for privacy)
		- Both the encrypted secret key and the encrypted combination of message and digest are sent together
		- ![image.png](../assets/image_1642575363364_0.png){:height 323, :width 553}
	- ## Steps taken by PGP at the receivers side
		- The receiver receives the combination of encrypted key and message digest
		- It decrypts the secret key using the receivers private key
		- Then it decrypts the message digest using the above obtained one time secret key
		- The digest is then decrypted using the senders public key to get the hash of the original message
		- Hash of the message and the decrypted digest is compared
		- ![image.png](../assets/image_1642575481543_0.png){:height 329, :width 524}
	- ## E-mail compatibility
		- When using PGP we will have to send binary data
		- But email only supports text
		- So we have to encode the raw binary into printable ASCII characters
		- Uses the radix-64 algorithm.
	- PGP message format
		- ??
- # Secure/Multipurpose Internet Mail Extension (S/MIME)
	- It is a security enhancement for MIME mail
	- Original internet RFC822 mail was text only
		- MIME provided support for varying content types and multipart messages
	- RFC822
		- Defines format for text messages sent using email
		- Messages have
			- Envelop
			- Contents
		- RFC822 applies only to contents
		- Contents consist of
			- header field
			- blank line
			- body
		- Limitations of RFC822
			- Can't transmit exe files or binary objects
			- National language characters not supported
			- Limited to certain size
			- Doesn't support non textual data
	- MIME
		- MIME includes
			- Five headers
			- A number of content formats
			- Transfer encoding for protection against alteration
		- Header fields are
			- MIME version
			- Content-Type
			- Content-Transfer-Encoding
			- Content-ID
			- Content-Description
		- MIME transfer encoding
			- To provide reliable delivery
			- Two methods of encoding data
				- Quoted printable
				- Base 64
	- ## S/MIME Functions
		- ??
	- ## S/MIME Cryptographic Algorithms
		- Digital signatures: DSS & RSA
		- Hash function: SHA-1 & MD5
		- Session key encryption: RSA
		- Message encryption: AES, triple-DES
	- ## S/MIME Message preparation
		- S/MIME secures a MIME entity with a signature, encryption or both
		- The MIME entity plus thee security related data are processed by S/MIME to produce a PKCS object
			- PKCS: Public Key Cryptography Specification
		- This PKCS object is then again wrapped in a MIME
	- ## S/MIME Content types
		- Enveloped data
		- Signed data
		- Clear-signed data
		- Registration request
		- Certificate only message
		- ### Enveloped data
			- Generate a pseudo random session key for a particular symmetric encryption algorithm
			- For each recipient, encrypt the session key with the recipients public RSA key
			- For each recipient, prepare a RecipientInfo block containing recipient's public key certificate, id of algo used to encrypt the session key, encrypted session key
			- Encrypt the message content with the session key
		- ### Signed data
			- ??
		- ### Clear Signing
			- Achieved using the multipart content type with a signed subtype
			- message content is clear, not encoded
			- signature part is encoded
		- ### Registration request
			- ??
		- ### Certificate only message
			- ??
- # IP Security
	- IPsec (Internet Protocol Security) is a framework that helps us to protect IP traffic on the network layer
		- IP is used for routing packets in the network layer.
		- However IP itself does not have any security features at all.
	- IPsec can protect our traffic with the following features
		- Confidentiality
		- Integrity
		- Authetication
		- Anti-replay
			- Prevents attackers from reusing packets by using sequence numbers and filtering duplicates
	- ## What is IPSec used for?
		- It is used for protecting sensitive data such as financial transactions, medical records, etc
		- It is used to secure VPN; IPSec tunnelling encrypts all data sent between two endpoints
		- IPSec also encrypts application layer data and provide security to the routers sending routing data across the public internet.
	- ## IPSec applications
		- Secure remote access over the internet
		- Enhancing the ecommerce security
	- ## Benefits of IPSec
		- In a firewall/router provides strong security to all traffic crossing the perimeter
		- Secures routing architecture
		- Is below transport layer, so transparent to applications
		- Can provide security for individual users
	- ## IP Security Architecture
		- IPSec specification consists of numerous documents
		- Two protocols are used to provide security
			- Authentication protocol by Authentication Headers(AH)
			- Combined encryption/authentication protocol by Encapsulating security payload (ESP)
		- IPSec specification is quite complex and consist of the following documents
			- ### Architecture
				- Covers general concepts, security requirements, definitions & mechanisms defining IPSec technology
			- ### Encapsulating Security Payload(ESP)
				- Packet format
				- Provides data integrity, authentication, anti-replay & encryption
			- ### Authentication Header(AH)
				- Packet format
				- Provides data integrity, authentication, anti replay but not encryption
				- Since ESP provide message authentication, AH is depreciated.
			- ### Internet Key Exchange(IKE)
				- Provides message content protection
			- ### Key Management Protocol(ISAKMP)
				- Tells how the set up of security association (SAs) and
				- how direct connections between two hosts that are using IPSec
	- ## IPSec services
		- Access control
		- Connectionless integrity
		- Data origin authentication
		- Rejection of replayed packets
		- Confidentiality (Encryption)
		- Limited traffic flow confidentiality
	- ## IP Security Policy
		- A security policy is applied to each IP packet that transmits from source to a destination
		- IPsec policy is determined by interaction of two databases
			- Security association database (SAD)
			- security policy database (SPD)
		- ### Security Associations
			- Association is a one way relationship between sender and receiver that affords security to all the traffic carried on it.
			- Each SA is uniquely identified by
				- Security parameters Index
				- IP destination address
				- Security protocol identifier
			- Security Association Database
				- Sequence number counter
				- Sequence counter overflow
				- Anti replay window
				- AH information
		- Security Policy Database
			- IPSec provides good flexibility in discriminating between the traffic
				- that needs IPSec protection and
				- that is allowed to bypass IPSec
			- SPD entry called selectors are used to filter outgoing traffic for a particular SA
			- SPD selectors
				- Destination IP address
				- Source IP address
				- UserID
				- Next layer protocol
				- Source & destination ports
	- ## IPSec protocol Mode
		- Both AH & ESP support two modes of operation
			- Transport mode
			- Tunnel mode
		- The main difference between the two is that transport mode uses the original IP header
			- while in tunnel mode, we use a new IP header
		- ### Transport Mode
			- It retains the original IP header
			- It only encapsulates the IP body and no the IP header
			- The payload data transmitted within the original IP packet is protected, but no the IP header
			- In transport mode, encrypted traffic is sent directly between two hosts that previously established a secure IPsec tunnel
			- Since no new IP header is created, it is less complex that tunnel mode
		- ### Tunnel Mode
			- Original IP packet is encapsulated to become the payload of a new IP packet
			- New IP header is created.
			- It is useful for protecting traffic between different networks
	- ## Authentication Headers (AH)
		- Provide support for data integrity & authentication
			- end system/router can authenticate user/app
			- prevents address spoofing attacks by tracking sequence numbers
		- Parties must share a secret key
		- Format
	- ## Anti Replay Service
		- ??
	- ## Encapsulating Security Payload (ESP)
		- Provides message content confidentiality & limited traffic flow confidentiality
		- Can optionally provide same authentication services as AH
		- Payload format ??
		- ### Transport mode ESP
			- Used to encrypt and optionally authenticate IP data
			- Data protected but header left clear
		- ### Tunnel Mode ESP
			- Protects both data and header
			- Adds new header for next hop
			- Good for VPNs, gateway to gateway security
	- ## Combining Security Associations
		- An individual SA can provide either an AH or ESP, but not both
			- sometimes both are required
		- For that multiple SAs must be employed for the same traffic flow called Security Association Bundle
		- Security Association Bundle refers to a sequence of SAs through which traffic must be processed to provide a desired set of IPsec services.
		- ### Combining security associations
			- SAs can be combined into bundles in two ways
				- Transport adjacency
					- Applying AH and ESP to the same IP packet without tunneling
					- Tunnelling is a protocol that allows for the secure movement of data from one network to another.
				- Iterated tunneling
					- Multiple layers of security protocols through IP tunnelling
	- ## Key Management
		- It is the section of IPSec that deals with the determination and distribution of secret keys
		- Two types of key management
			- Manual
				- A system administrator manually configures each system with its own keys and with keys of the other communicating systems
			- Automatic
				- An automated system that enables on demand creation of keys for SAs and facilitates the use of keys
		- ### ISAKMP/Oakley
			- It is the automated key management protocol for IPSec
			- It consists of the following elements
				- Oakley Key Determination Protocol
				- Internet Security Association and Key Management Protocol (ISAKMP)
			- #### Oakley Key Determination Protocol
				- Key exchange protocol based on Diffie-Hellman algorithm with added security
				- Advantages of Diffie-Hellman
					- Secret keys are created only when needed
					- Exchange requires no pre existing infrastructure
				- Disadvantages
					- Prone to man in the middle attacks
					- Does not provide information on identities of the parties
					- computationally intensive: clogging attacks
						- when the computer is busy computing the diffie hellman, a number of bogus requests from false source IPs can be placed.
						- Stopping such requests is difficult, ignoring them cause DOS
				- **Oakley Features**
					- It employs cookies to thwart clogging attacks
						- Thwart; not prevent
						- Having cookies will allow use to identify the sender. This can prevent the bogus requests problem. [source](https://www.cs.wm.edu/~ksun/csci454-s16/notes/T08.2_INetKeyMan-6spp.pdf)
					- It enables two parties to negotiate a group; this specifies the global parameters of the Diffie-Hellman
					- It uses nonces to ensure against replay attacks
					- It authenticate Diffie-Hellman exchange to thwart man in the middle attacks
				- **Oakley Authentication Method**
					- Digital Signatures
					- Public key encryption
					- Symmetric key encryption