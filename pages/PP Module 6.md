public:: true

- ![Module 6 by Miss.pdf](../assets/Module_6_by_Miss_1643195193539_0.pdf)
- Questions
	- What is a thread pool in java? What is its use?(4)
	- What is busy waiting? What is its principal alternative?(4)
	- What do you mean by late binding of machine code? What are its advantages
	  and disadvantages?(6)
	- Write short notes on Virtual Machines.(3)
	- What is quasi parallelism?(3)
	- Explain busy wait synchronization.(6)
	- What is a thread pool in Java? What purpose does it serve?(4)
	- In what sense is fork/join more powerful than co-begin?(4)
	- What is a semaphore? What operations does it support? How binary and general
	  semaphore does differ?(6)
	- Describe six different mechanisms(principles) commonly used to create new
	  threads of control in a concurrent program(9)
	- What is a JIT compiler? What are its potential advantages over
	  interpretation/conventional compilation? (3)
	- Remaining 2019 may, 2019 dec
- Syllabus
	- Concurrency:- Threads, Synchronization. Run-time program Management:- Virtual Machines, Late Binding of Machine Code, Reflection, Symbolic Debugging, Performance Analysis.
- A program is concurrent if it has more than one active execution context
- A system is concurrent if two or more tasks are executed at same time.
- If threads are executing in different processes it is said to have parallelism
- # Communication and Synchronization
	- Communication refers to any mechanism that allows one thread to obtain information produced by another
	- Two mechanisms
		- Message Passing
			- A thread can communicate with another by explicit send operation
		- Shared memory
			- A thread can communicate with others by writing to a common variable
	- Synchronization is any mechanism than allows the programmer to control the relative order in which operations occur in different threads
	- Two mechanisms
		- Spinning (busy waiting)
			- Threads runs a loop until some condition becomes true
			- eg: until a message queue becomes empty
		- Blocking
			- In blocking synchronization, the waiting thread voluntarily relinquishes it processor to some other thread
			- A thread that makes the condition true in the future will take action to make the blocked thread run again
- # Languages and Libraries
	- Thread level concurrency can be provided by
		- explicit concurrent languages
		- compiler supported extensions
		- library packages
	- All parallel programming use libraries for synchronisation or message passing
	- UNIX
		- Shared memory parallelism using POSIX pthread standards
	- Windows
		- Win32 thread package
	- Language support for concurrency was provided in Algol 68 and Ada
	- Fortran has parallel execution of loops using OpenMP
	- Concurrent languages is more advantageous than library packages in compiler support
- # Thread Creations
	- All concurrent systems allows threads to be created dynamically
	- ## Co-begin
		- Normally a compound statement call for sequential execution of constituent statements
			- But a co-begin construct calls instead for concurrent execution
		- All statements run concurrently
		- ```algo-68
		  co-begin
		  	stmnt1
		      stmnt2
		      ....
		      stmnt3
		  end
		  ```
		- Co-begin is the principal means of creating threads in Algol-68
		- This appears in other systems, such as OpenMP
		- ```c
		  #pragma omp sections 
		  { 
		    # pragma omp section 
		    { 
		      printf("thread 1 here\n"); 
		    }
		  	# pragma omp section 
		    { 
		      printf("thread 2 here\n"); 
		    }
		  }
		  ```
	- ## Parallel Loops
		- Loops whose iterations are to be executed concurrently
		- Many systems including OpenMP, Fortran, .NET etc provide support from parallel loops.
		- ```c
		  #pragma omp parallel for
		  for(int i =0; i<3; i++){
		    printf("thread%d here \n", i);
		  }
		  ```
	- ## Launch at Elaboration
		- Declare a thread as a procedure without parameters. When declaration is elaborated, the thread is created to execute the code.
		- In Ada threads are called Tasks(T)
	- ## Fork / Join
		- Co-begin, parallel loops and launch at elaboration all lead to a concurrent control flow pattern in which thread executions are properly nested.
		- Parallel loops are data parallel. Co-begin and launch at elaboration are task parallel.
		- The fork makes the creation of threads an explicit, executable operation
		- Join allows a thread to wait for the completion of a previously forked thread.
		- ### Thread Pools in Java
			- Thread pools reuse previously created threads to execute current tasks
			- It offers a solution to the problem of thread cycle overhead and resource thrashing.
			- Since the thread exists when the resource arrives, the delay introduced by thread creation is eliminated
			- Java provides the Executor framework which has the Executor interface, ExecutorService sub interface & the ThreadPoolExecutor class.
			- It allows you to focus on the task at hand rather than the thread mechanics.
	- ## Implicit Receipt
		- ??
	- ## Early Repy
		- ??
- # Implementation of Threads
	- threads of a concurrent program are written on top of processes provided by the OS
	- We can either use
		- a separate OS process for every thread or
			- Drawback: too expensive
		- multiplex all of a program's threads on top of a single process
			- Drawback: if current thread makes a system call that blocks then no other thread can run.
	- Another approach is to use a intermediary approach
		- Large number of thread are run on top of small number of processes
	- Threads via coroutines
		- Implemented using coroutines
		- Done using 3 steps
			- First the argument to transfer is hidden by implementing a scheduler which chooses the next thread to run when the current thread yields the processor
			- Then we implement a preemption which suspends the current thread and gives chance to other threads to run
			- We allow the data structures that describe our collection of threads to be shared by more than one OS process, so that the threads can run on any of the processes.
	- ??
	-
	-
- # Synchronization
	- Controls the order in which operations occur in different threads
	- Used to eliminate race around conditions
	- Done by two mechanisms
		- Blocking
		- Busy waiting or spinning
	- Synchronization can be
		- Mutual Exclusion
			- Making sure that only one process is executing a critical section at a time
		- Condition Synchronization
			- Making sure a process does not proceed until some condition holds
	- ## Busy - Wait Synchronization
		- Technique in which a process repeatedly checks to see if a condition is true
		- ### Spin locks
			- This is a lock that cause the thread trying to acquire it to wait in a loop while repeatedly checking if the lock is available
			- Test and set instruction writes 1(set) to the memory location and returns its old value as a single atomic
			- Calling process acquires the lock if the old value was 0 otherwise it busy waits
			- Disadvantage
				- When p1 has the lock, and p2 is waiting for the lock, p2 will keep on incurring the bus transactions in attempts to acquire the lock
				- This increases the bus traffic requirement, slows down all the other traffic
			- Test and test and set
				- It spins with ordinary reads until it appears that the lock is free
			- Backoff
				- When a thread tries to acquire the lock and fails, it waits sometime before retrying.
		- ### Barriers
			- Barrier for a group of threads makes sure that every thread completes the previous step before any moves on to the next. A Barriers serves to provide this synchronization
			- Implemented using a shared counter modified by an atomic fetch and decrement instruction
			- Counter starts at n
			- it is decremented as each thread reaches a barrier
			- When counter becomes 0, it allows other threads to proceed.
	- ## Non Blocking Algorithms
		- ??
	- ## Scheduler based synchronization
		- The problem with scheduler based synchronization is that it consumes processor cycles.
		- Busy wait synchornization makes sense only if
			- one has nothing better to do with the current processor
			- or the expected wait time is less than the time context switch to some other thread and switch back
	- ## Semaphore
		- Semaphores are the oldest of the scheduler based synchronization mechanisms
		- Semaphore is basically a counter with two associated operations, P and V
		- Thread that calls P atomically decrements the counter then waits until it is non negative
		- A thread that calls V atomically increments the counter and wakes up a thread, if any
		- P acquires the lock, V releases it
- # Runtime Program Management
	- ## Virtual Machines
		- A virtual machine provides a complete programming environment
		- Its API includes everything required for correct execution of the programs that run above it.
		- Either system VMs or process VMs
		- System VM emulates all the hardware facilities needed to run a standard OS
		- Process VM used to increase portability of programs
		- ### Java Virtual Machine
			- JVM provides support for all built in and reference types defined by Java
			- Java compiler, javac, tranforms a .java source file into a .class file that is written in java bytecode, which is the machine language for an imaginary machine know as JVM
			- Working
				- Initially the name of the class file containing the static method main is given to JVM
				- Loads this into memory
				- Verifies if it satisfies required constants
				- Allocate static fields
				- Links to preloaded libraries
				- calls main in single thread
			- Storage management
				- Has constant pool
				- set of registers
				- stack of each thread
				- method area to hold executable byte code
				- heap for dynamically allocated objects
			- Global Data
				- The constant pool contains both program constants and a variety of symbol table information needed by JVM and other tools
			- Per-thread data
				- A program running on the JVM begins with a single thread
				- Additional threads are created using the build in class Thread
				- ??
			- Class files
				- A JVM class file is stored as a stream of bytes
			- Byte code
				- Byte code for a method appears in an entry in the class files method table
			- ?? NEED TO STUDY LAST FEW SLIDES
			  background-color:: #793e3e
	- ## Late binding of machine code
		- Compiler produces a target code in machine language that can be executed several times for different inputs
		- In some environments the translation and execution are combined.
		- Many systems delay the binding of a program to its machine code
			- Just in time compilers
		- ### Just in Time (JIT) Compiler
			- It is an essential part of JRE (Java Runtime Environment)
			- responsible for performance optimization
			- used to increase efficiency of interpreter
			- Compiles programs immediately prior to execution
		- ### Dynamic Compilation
			- In some cases JIT compilation must be delayed because
				- the source code or byte code was not discovered until run time
				- we wish to perform optimizations that depend on information gathered during execution
				- In these cases we say that the language implementation employs dynamic compilation
			- Common lisp uses dynamic compilations
			- Dynamic compilation can use statistics gathered by run time profiling to identify hot paths which it can then optimize in the background
		- ### Binary Translation
			- Process of recompilation of object code
			- It allows already compiled programs to be run on a machine with different instruction set architecture.
			- Principle challenge is the loss of information in the original source to byte code
		- ### Dynamic Optimization
			- Revisits hot paths and optimize them aggressively.
		- ### Binary Rewriting
			- Technique to modify existing executable code (object code)
			- Used by profiling
				- count the number of times that each subroutine is called or the number of times each loop iterates
			- It can be used to simulate new architectures
		- ### Mobile Code
			- Code in byte code or scripting languages are mostly compact and machine independent
			- it can be easily moved over the internet and run on almost any platform
		- ### Sandboxing
			- For code to be mobile, it should be executed in some sort of sandbox
			- Sandboxing is a software management strategy that isolates applications from critical system resources and other programs
			- It provides an extra layer of protection
	- ## Inspection/Introspection
		- ### Reflection
			- Allowing a program to reason about its own internal structures and types is called introspection
			- Java and C# provide similar functionality through a reflection API that allows a program to persue its own metadata
			- Useful for print diagnostics
			- Java 5 Reflection
				- Java's root class is Object
				- Object supports getClass method which returns java.lang.class
				- It supports operations like getName and getParameters method
				- ```java
				  int[] A = new int[10];
				  System.out.println(A.getClass().getName()); // prints "[I" 
				  String[] C = new String[10];
				  System.out.println(C.getClass().getName()); // "[Ljava.lang.String;" 
				  Foo[][] D = new Foo[10][10];
				  System.out.println(D.getClass().getName()); // "[[LFoo;"
				  ```
			- C# Reflection
				- It is similar to java
				- System.type is analogous to java.lang.class
		- ### Symbolic Debugging
			- Built into most programming language interpreters, virtual machines, and integrated program development environments
			- They are also available as stand alone tools, most famous being GNU's gdb
			- The adjective symbolic refers to the debugger's understanding of high level language syntax
			- The debugger allows the user to perform two main kinds of operations
				- A breakpoint specifies that execution should stop if it reaches a particular location in the source code
				- A watchpoint specifies that execution should stop if a particular variable is read or written
		- ### Performance Analysis
			- Simplest way to measure the amount of time spent in each part of the code is to sample the program counter periodically
			- Classic prof tool in Unix uses this approach
			- After execution the tool provides statistical summary of the percentage of  time spent in each subroutine and loop.
			- Limitations of Prof tool
				- Results are only approaximates
				- Could not capture fine grain costs
			- gprof tool
				- This tool relies on compiler support to instrument procedure prologues
				- It logs the number of times D is called from each location
		- Run time system
			- It refers to the set of libraries on which the language implementation depends for correct operation
			- C has small run time systems
			- C# depends heavily on runtime systems