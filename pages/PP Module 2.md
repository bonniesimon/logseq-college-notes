public:: true

- [[PP Module 1]]
- ![Module 2.1 by Miss.pdf](../assets/Module_2.1_by_Miss_1637170611645_0.pdf)
- ![Module 2.2 by Miss.pdf](../assets/Module_2.2_by_Miss_1637170616941_0.pdf)
- Data types
	- it is a classification that specifies what type of value a variable has
		- and what type of mathematical relational or logical operations can be applied on it without causing error
	- Two purpose
		- To provide implicit context for many operations
		- Types limit the set of operations that may be performed
			- eg: adding a character and array
- Type system
	- consists of a mechanism to define types
		- and to associate language constructs with them
	- a set of rules for type equivalence, type compatibility, type inference
		- Type equivalence: To check whether types of two values are equal
		- Type compatibility: To check whether a value of the type can be used in the given context
		- Type Inference: rules to define the type of an expression based on its constituent parts or the surrounding context
	- Subroutines also have types in some languages
		- when they are passed as argument, returned by functions and stored in variables.
- Type checking
	- process of ensuring that the program obeys the languages type compatibility rules
	- A violation of the rules : type clash
	- Strongly Typed
		- a language that prohibits any operation to any object that is not intended to support that operation
		- Variables are necessarily bound to a particular datatype
		- Eg: ada, cpp
	- Weakly Typed
		- Variables are not of a specific data type
		- Variables have datatypes, but are not bound to a specific data type
		- eg: php, javascript
	- Statically Typed
		- A language is said to be statically typed if it is strongly typed and type checking happens at compile time
		- variables are declared before they are used
		- eg: java, c, cpp
	- Dynamic Typing
		- Type checking happens at runtime
		- most scripting languages
		- eg: lisp, smalltalk
	- Polymorphism
		- allows a single body of code to work with objects of multiple types
- Types
	- types can be categorized in 3 ways
		- Denotational
		- Constructive
		- Abstraction based
- Classification of Types
	- ??
- Type Checking
	- In a statically typed language, every definition of an object must specify the object's type
	- Type equivalence, type compatibility, type inference
	- Type checking can be static or dynamic
	- Type Equivalence
		- Two types
			- Name Equivalence
				- Two types are equal if and only if they have the same name
			- Structural Equivalence
				- Two types are equal if and only if they have the same structure
					- same name and same type definitions in the correct order
			- Ada follows name equivalence while ML follows structural equivalence
	- Type compatibility
		- a values type must be compatible with that of the context it appears in
		- 3 types
			- Assignment compatibility
			- Expression compatibility
			- Parameter compatibility
	- Type inference
		- automatic detection and deduction of data type of an expression
		- usually done at compile time
		- involves analysing a program & inferring the different types used
		- Subranges
			- ??
		- Composite types
			- ??
		- ML Type system
			- Type inference occurs in certain functional languages like ML
			- programmers can leave out the types and the compiler will infer the types of certain objects
	- Type Conversion
		- conversion of a value from one data type to another
		- either implicit or explicit
		- Implicit : type cohersion
		- explicit : type casting
		- Non Converting type cast
			- A change in type that does not alter the underlying bits
- ## Records
	- Heterogeneous collection of data for storage and manipulation
		- Algol68, C, Cpp , LISP : structure
		- Fortran: types
		- Java doesn't support structure
		- Csharp uses reference model for class types and value model for struct types
	- Record in c
	  ```
	  struct element{
	      char name[2];
	      int atomic_numbr;
	      double atomic_weight;
	      _Bool mettallic;
	  }copper;
	  ```
	- Record in Pascal
	  ```pascal
	  type two_chars = packed array[1..2] of char;
	  type element = record
	      name: two_chars;
	      atomic_number: integer;
	      atomic_weight: real;
	      mettalic: Boolean;
	  end;
	  ```
	- ### Accessing Record field
		- Each component in a record is called a field
		- to access a field many languages use dot notation
			- In c
			  ```
			  struct element copper;
			  copper.name[0] = 'C';
			  copper.name[1] = 'u';
			  copper.atomic_number = 92;
			  ```
				- Pascal uses same dot notation
			- Fortran 90
			  ```
			  copper%name
			  copper%atomic_weight
			  ```
			- Cobol & Algo 68
			  ```
			  name of copper
			  atomic_weight of copper
			  ```
			- ML
			  ```
			  #name copper
			  #atomic_weight copper
			  ```
			- Common Lisp
			  ```
			  (element-name copper)
			  (element-atomic_weight copper)
			  ```
	- Nested Records
		- many languages allow nested records
		- check ppt
		- In ML the order of record fields is insignificant while accessing them
	- ### Memory Layout
		- The fields of a record are stored in adjacent memory locations
		- symbol table keeps track of the offset of each field in the record
		- to access a field, a load and store instruction with displacement addressing is used.
		- ![image.png](../assets/image_1637221632265_0.png)
			- name takes two bytes since it has only two characters
			- atomic number takes 4 bytes. So there is a hole in between name and atomic number
			- metallic is a boolean variable which occupies only 1 byte, hence 3 byte of remains as a hole
		- Value Model & Reference Model
			- In value model, the value will only be copied
			- In reference model, the reference that an object points to is copied.
		-
	- Packed Types
		- Pascal allows packed records
		- ```pascal
		  type element = packed record
		      name: two_chars;
		      atomic_number: integer;
		      .....
		  end;
		  ```
		- packed records pushes the fields together without holes
		- it indicates to the compiler to optimize for space than speed
		- Layout of Packed records
			- ![image.png](../assets/image_1637222282668_0.png)
		- Holes
			- waste space
			- packed records eliminate holes but access time increases
				- solution is to sort according to size
				- one byte fields come first
				- then half-word(two byte)
				- word aligned fields
				- double word aligned fields
				- ![image.png](../assets/image_1637222383259_0.png)
	- With statements
		- manipulating fields with many nested records is difficult
		- pascal provides the with statement to simplify it
	- Variant Records
		- a variant record(unions) is a record in which only one field is valid at a time
		- fields acts as alternative
		- each of fields is itself called variants
		- proposed during the era of memory restrictions
		- variables share the same bytes in memory
		- size would be the size of the largest element
- ## Arrays
	- Arrays are homogeneous data types
	- mapping from an index type to a component or element type
		- fortran requires index type to be integer
		- Ada & pascal allow it to be discrete types
	- Syntax
		- elements can be accessed by appending a subscript delimited by parenthesis or square brackets
		- ```
		  //ada or fortran
		  A(3)
		  //  perl & C
		  A[3]
		  ```
	- Declarations
		- In C
		  ```
		  char upper[20];
		  ```
			- Lower bound of index is 0
				- indices are 0 to n-1
		- In fortran
		  ```fortran
		  
		  ```
			- Lower bound index is 1
		- In some languages an Array constructor is used
		- In pascal
		  ```
		  var upper: array['a'..'z'] of char
		  ```
		- In Ada
		  ```
		  upper: array(character range 'a'..'z') of character;
		  ```
	- Multidimensional arrays
		- egs in ppt
	- Slices and Array operations
		- in most languages, only assignment and selection operations are permitted on arrays
		- Ada & Fortran 90 allow arrays to be compared for equality
		- Ada allows lexicographic ordering comparison between two arrays
		- Fortran has a very rich set of array operations
			- built in operations that take arrays as arguments
			- A+B is an array in which each element is the result of the sum of corresponding elements in A and B
			- tan(A) returns the tangents of A
	- Dimensions, Bounds & Allocation
		- Static allocation for arrays whose lifetime is the entire program
		- stack allocation for arrays whose lifetime is an invocation of a subroutine
		- Heap allocation for dynamically allocated arrays
		- storage management is complex for arrays who's shape is not defined until elaboration time and might change during execution
	- Dope Vectors
		- Used to hold shape information at run time
			- Dope vectors contain the lower bound and size of each dimension
		- on compilation, symbol table maintains bounds and dimensions for every array
		- If array dimensions are statically known, then compiler can simply check the symbol table to compute the bounds
		- Else if it is not statically known, then the compiler should look them up in a dope vector at run time
	- Stack Allocation
		- ??
	- Heap Allocation
		- ??
	- Memory Layout
		- Arrays store items in contiguous locations
		- In 1D array, second item is stored immediately after the first item
		- But in a 2D array, where to store the second item
			- Row major order
			- column major order
		- Row Major order
			- a[2, 4] is followed by a[2, 5]
			- consecutive locations in memory hold elements that differ by one in the final subscript
		- Column major order
			- a[2,4] is followed by a[3,4]
			- consecutive locations in memory hold elements that differ by one in the initial subscript
	- Row-Pointer Layout
		- elements of an array need not be stored in contiguous memory locations
		- they can lie anywhere on memory
		- an auxiliary array of pointers to the elements is created
		- requires more space
		- Advantages
			- access is faster sometimes
			- allows rows to be of different length
				- no holes. such arrays are called jagged arrays
		-
	- Address Calculations
		- ??
- Pointers & Recursive Types
	- A recursive type is one whose objects reference objects of the same type
	- Records, lists, trees are some examples
	- In reference model
		- a record type of foo to include reference to another record type foo
	- In value model
		- recursive types require pointers
		- a variable whose value is a reference to some object
	- Syntax and Operatios
		- Operations on pointers include
			- allocation and deallocation of objects in the heap
			- dereferencing pointers to access the objects
			- assignment of one pointer to another
		- Behaviour of these operations depends on whether the language is
			- functional or imperative
			- reference or value model for variable names
		- Functional models use reference model for names
		- Imperative languages use value model or reference model
		- In ML, the datatype mechanism is used to declare recursive types
		- Tree implementation in ML
			- ??
- Pointers & Array
	- pointers and arrays are closely linked in C
	- Array names are automatically converted to pointers to the arrays 1st element
- Multidimensional Arrays
	- `int *a[n]` - allocates space for n row pointers
	- `int a[n][m]` - allocates space for 2D array
	- when array used as parameter in function call
		- 1D with formal parameters  - `int a[]` or `int *a`
		- 2D with formal parameter with row pointer layout - `int *a[]` or `int **a`
		- 2D with formal parameter with contiguous layout - `int a[][m]` or `int(*a)[m]`
- Dangling References
	- when a heap allocated object is no longer live, object's space must be reclaimed
	- stack objects are reclaimed by epilogue
	- heap objects are reclaimed in two ways
		- directly by programmer
		- garbage collector
- Garbage collector
	- results in memory leaks
	- dangling references
	- explicit reclamation of heap objects may create bugs
	- when object no longer live, reclaim it automatically
	- Garbage collection algorithms are of two types
		- Reference counting
			- Disadvantages
				- Circular structures will always have ref count = 1. Thus they can't be freed
				- Overhead associated with incrementing and decrementing ref count.
		- Tracing collection
			- An object is useful if it can be reached from a chain of valid pointers from a root object.
			- Types
				- Mark & Sweep
				- Pointer reversal
				- Stop & copy
				- Generational collection
				- Conservative collection
			- Mark & Sweep
				- Executed when the amount of free space in heap falls below some threshold
				- 3 steps are executed by garbage collector
				- 1. Collector walks through the heap and marks every block as useless
				  2. Collector recursively explores all linked data structures in the program marking each newly discovered block as useful
				  3. Collector again walks through the heap, moving moving every block that is still marked useless to free list
			- Pointer Reversal
				- Step 2 of mark & sweep is recursive
				- This requires a stack and hence has a memory requirement which might not be available if we are running out of memory.
				- So in pointer reversal, as the collector explores paths, it reverses the pointer it follows
				- So each block points to its previous block
				- Disadvantage
					- While GC is traversing, the program execution has to stop since we are altering the DS's
					- Extra bits to keep track of which bits are already processed.
			- Stop & Copy
				- In languages with variable size heap lock, GC reduces external fragmentation by storage compaction. Converts small free memory blocks into large free memory blocks.
				- It divides the heap into 2 regions of equal size
				- All allocation happens in the first half.
				- When it is full, the collector explores the half and finds reachable DS's
				- Then each reachable block is moved to the second half.
				- After the collector is done, all useful blocks in the second half and useless ones remain in the first half.
				- Collector can swap the first and second halves.
			- Generational Collection
				- Used to further reduce cost of collection
				- Assume that most dynamically allocated objects are short lived.
				- Heap is divided into multiple regions
				- New objects re allocated in young region
				- Surviving young objects are promoted to old generation region
				- When space is low, collector examines the youngest region.
			- Conservative Collection
				- This works without compiler cooperation
				- Assumes that anything must be a pointer leaving a lot of garbage uncollected.
				- Requires that the running program stop and wait for the collector to finish before proceeding.
- Reference count
	- counter tracks the number of pointers that refer to the object
	- when count is 0, its object can be reclaimed
	- ??
- Mark and sweep
	- ??
- Lists
	- ??