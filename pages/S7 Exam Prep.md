- # Timetable
	- 09/02/22 CG
	- 15/02/22 PP
	- 18/02/22 CSA
	- 21/02/22 DC
	- 23/02/22 CNS
	- 25/02/22 ML
	- 02/03/22 DIP(H)
- # Resources
	- [annu12340 questions and video](https://github.com/annu12340/KTU-S7)
- # ML
	- Module 1
		- Finished till VC dimension
		- Remaining
			- Applications of ML
	- Module 3
		- Remaining
			- Polynomial regression & least square methods
	- Module 4
		- Remaining
			- Regression trees
			- Back propagation algorithm
	- Module 5
		- Reached HMM
	- Module 6
		- Remaining
			- Bimodal mixture
			- Expectation maximization
			- DIANA
- # CG
  collapsed:: true
	- Module 1
		- Remaining
			- Input devices
			- Raster scan and random scan systems
	- Module 3
		- Remaining
			- Quadratic Surfaces
	- Module 5
		- Remaining
			- Visible surface - Scan Line Method
	- Module 6
		- Try out histogram matching & equilization problem
- # PP
  collapsed:: true
	- Module 5
		- Finished scripting languages except Regex part
		- Remaining
			- Regex
			- Modules in OOP
	- Module 6
		- Remaining
			- Runtime program management
	- Module 4
		- Skimmed through main topics
	- Module 3
		- Except error handling (kinda skimmed through)
	- Module 2
		- Remaining
			- Address calculation
- # CSA
  collapsed:: true
	- Module 1
		- Remaining
			- Numericals
			- Amdahl's law equations
			- Name of the figure asked in first series exam that I got wrong
			- Detection of parallelism
	- Module 2
		- Remaining
			- RISC processors
	- Module 3
		- Remaining
			- Wired Barrier Synchronization
			- Snoopy Bus Protocols
	- Module 4
		- Remaining
			- Latency analysis of Wormhole and store & forward
	- Module 5
		- Took notes
		- Remaining
			- Internal data forwarding example
			- Flow diagrams in Hazard avoidance
			- Floating point operations
			- Arithmetic Pipeline Design
			- Superscalar pipeline scheduling
	- Module 6
		- Took notes and skimmed through. Last few parts remaining
		- Remaining
			- Sharing list creation
			- Distributed cacheing example
- # DC
collapsed:: true
	- Module 1
		- Remaining
			- Trends in DS
	- Module 2
		- Remaining
			- Proxy pattern, brokerage pattern, reflection
			- Security Model
	- Module 3
		- Remaining
			- Group membership management onwards
	- Module 4
		- Remaining
			- Kerberos
			- Cache consistency onwards
	- Module 5
		- Studied till lock implementation
		- Remaining
			- Deadlock onwards
	- Module 6
		- Remaining
			- Bully Algorithm
- # CNS
collapsed:: true
	- Module 3
		- Remaining
			- Elliptic Curve
	- Module 4
		- Remaining
			- MD5 algorithm
	- Module 5
		- Finished pdf 5.1
		- Finished module 5.
		- Need to learn the packet formats and all
		- Remaining
			- Anti replay window
			- key management
	- Module 6
		- Everything except last few topics (marked in the md note)
		- Remaining
			- Firewall