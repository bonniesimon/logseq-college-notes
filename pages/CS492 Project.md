- Ideas
  collapsed:: true
	- Travelling salesman problem using quantum computing
	- DP-T3
- [[Schizophrenia Diagnosis using EEG]]
- [[Abstract Phase 1]]
- [[Implementing new model 4 Apr]]
- [[Setting PPT Apr 4]]
- [[Srishti 22 Project Submission]]
- [[Model Parameters Notes]]
- [[component integration]]
- [[Getting output to file from arduino]]
- [[Schizo Backend]]
- [[Project Report Final]]
- 1250 * 19
	- we use LSTM when we have large values
	- LSTM can retain more information if there were large value
		- if 12500 instead of 1250 & 190 instead of 19 (purely examples)
- Meeting Notes
	- [[Project Meeting 10 sep 2021]]
	- [[Project Meeting 14 Sep 2021]]
	- [[Project Meeting 17 Sep 2021]]
	- [[Project Meeting 07 Oct 2021]]
	- [[Project Meeting 09 Oct 2021]]
	- [[Project Meeting 19 Oct 2021]]
	- [[Project Meeting 14 Mar 2021]]
- Archive
  collapsed:: true
	- Some papers for project
	  collapsed:: true
		- [Pistis: Issuing Trusted and Authorized Certificates With Distributed Ledger and TEE](https://www.computer.org/csdl/journal/td/2022/07/09582795/1xR2VbUJiEg)
		- [A Practical Framework for Secure Document Retrieval in Encrypted Cloud File Systems](https://www.computer.org/csdl/journal/td/2022/05/09524492/1wpqIyaJE9W)
		  id:: 61ae484b-869f-41d7-abba-c8cb76f63744
		- [A Quantum Approach Towards the Adaptive Prediction of Cloud Workloads](https://www.computer.org/csdl/journal/td/2021/12/09428529/1twaQ8N7os0)
			- [paper](https://sci-hub.hkvisa.net/10.1109/TPDS.2021.3079341)
		- [LightChain: Scalable DHT-Based Blockchain](https://www.computer.org/csdl/journal/td/2021/10/09397334/1sA4ZzzPF7O)
			- [paper](https://sci-hub.hkvisa.net/10.1109/TPDS.2021.3071176)
	- Research
	  collapsed:: true
		- [[Blockchain for Dummies]]