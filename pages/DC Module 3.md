public:: true

- ![Module 3 by Miss.pdf](../assets/Module_3_by_Miss_1639490273547_0.pdf)
- Sockets
	- Sockets provide an endpoint for communication between processes
	- Messages are transmitted between a socket in one process and a socket in another process
	- For a process to receive messages, it socket must be bound to a local port and one of the internet addresses
		- Same socket can be used for sending and receiving messages
	- Any process can make use of the multiple ports to receive message
		- but cannot share ports with other processes on the same computer
	- Each port is associated with a particular protocol - either UDP or TCP
# UDP Datagram Communication
	- No acknowledgement or retries
	- process must first create a socket
		- bound to an internet address of the local host and a local port
	- server will bind its socket to a server port
		- makes it known to the clients, so that they can send messages to it
	- client binds its socket to any free local port
	- The receive method returns
		- internet address and port of the sender
			- To allow the recipient to send a reply
		- the message
	- ### Issues
		- Message Size
			- Receiving process needs to specify an array of bytes
			- IP protocol packet lengths of up to 216 bytes
		- Blocking
			- Sockets normally provide non-blocking sends and blocking receives for datagram communication
		- Timeouts
			- timeouts can be set on sockets
			- choosing appropriate timeout interval is difficult
		- Receive from any
			- receive method doesn't specify an origin for the messages
	- ### Failure model for UDP datagrams
		- Omission failures
			- Messages may be dropped occasionally, because of
				- checksum error
				- no buffer space is available at the source or destination
			- Send omission & receive omission failures in the communication channel
		- Ordering
			- Messages can be delivered in out of order
	- ### Use of UDP
		- For some applications omission failures are acceptable
			- DNS is implemented over UDP
			- Voice Over IP is implemented over UDP
		- No overheads associated like guaranteed message delivery
		- overheads??
- # TCP Stream Communication
	- API to the TCP protocol provides abstraction of a stream of bytes to which data maybe written and read from
	- The following characteristics of the network are hidden by the stream abstraction
		- Message sizes
			- The application can choose how much data it writes to a stream or reads from it
			- On arrival the data is handed to the application as requested
		- Lost messages
			- TCP uses acknowledgement scheme
			- Buffers
			- Acknowledgements
			- Timeouts
			- Sliding window protocol
		- Flow control
			- Match the speed of the processes that read from and write to the stream
			- Fast sender slow receiver
		- Message duplication & Ordering
			- Message identifiers are associated with each IP packet
			- Recipient to detect and reject duplicates
			- Reorder messages that were delivered out of order
		- Message Destination
			- Establish a connection before they can communicate over a stream
			- Establishing connection involves the following steps
				- Connect request from client to server
				- Accept request from server to client
	- Client Role
		- Create a stream socket bound to any port
		- Making a connect request asking for a connection to a server at its server port
	- Server Role
		- Create a listening socket bound to a server port
		- Waiting for clients to request connections
	- Two streams in each pair of sockets
		- Input stream
		- Output stream
	- ### Issues
		- Matching Data items
			- Agreement of  the contents the data transmitted over a stream
			- Without proper cooperation receiving process
				- experiences errors when interpreting the data
				- may block due to insufficient data in the stream
		- Threads
			- Creates a new thread for each connection
			- Allows the server to block a client without interfering other connections
	- ### Failure Model
		- TCP uses timeouts and retransmission to deal with lost packets
		- Connection is declared broken if
			- packet loss over a connection passes a limit
			- network is severed
			- network becomes congested
			- TCP software responsible for sending messages receives no acknowledgement
	- Use of TCP
		- HTTP
		- FTP
		- Telnet
		- SMTP
- Java API for UDP
- Java API for TCP
- # External Data Representation & Marshalling
	- Any two computers can exchange binary data values if
		- The values are converted to an agreed external format
		- If the computers are of the same type, hence no conversion
		- Transmitted in the senders format but with an indication of the format
	- Marshalling
		- Assembling and converting a collection of data items for transmission
	- Unmarshalling
	- Three approaches
		- CORBA's common data representation
		- Java's object serialization
		- XML
# Remote Procedure Call
	- High level of distribution transparency
	- Extends the abstraction of a procedure call to distributed environments
	- Procedures in remote machines can be called as if they are procedures in the local system (or local address space)
	- RPC hides important aspects of distribution
		- Encoding and decoding of parameters and results
		- passing of messages
		- preserving of the required semantics for the procedure call
	- ## Design Issues for RPC
		- ### Programming with Interfaces
			- Communication can be by means of
				- procedure calls between modules
				- direct access to variables in another module
			- To control communication between modules explicit interface is defined for each module
				- Hide all information except which is available through its interface
				- So as long as the interface remains the same, the implementation can be changed without affecting the users
			- Service interface
				- the specification of procedures offered by a server
				- defines the types of the arguments of each of the procedures
			- Interfaces in distributed systems
				- Modules of a distributed program can run in sperate processes
				- In client server, server provides interfaces for the client to access procedures in the server
				- File system offers procedures to read and write files
			- Benefits of programming with interfaces
				- concerned only with abstraction and not with implementation
				- No need to understand the programming language or underlying platform used to implement the service
				- Support for software evolution
					- implementation can be changed as long as the interface remains the same
			- Nature of underlying infrastructure
				- Client running in one process does not have access to variables in another. No direct access
				- Parameter passing
					- No call by value or call by reference
					- Input and output parameters
				- Address cannot be passed as arguments
			- #### Interface Definition Languages
				- RPC mechanism can be integrated with a particular programming language if it
					- has adequate notation for defining interfaces
					- allows input and output parameters to be mapped onto the language's normal use
				- Interface definition languages (IDL) allow procedures implemented in different languages to invoke one another
		- ### RPC call semantics
			- Maybe semantics
				- Procedure executed maybe once or not at all
				- Used when no fault tolerance measures are applied
				- Can lead to following failures
					- Omission failures if the request or message is lost
					- Crash failures when server fails
				- Maybe semantics is useful only for application in which occasional failed calls are acceptable
			- At least once semantics
				- With this semantic, the receiver receives either the result, in which case he knows the procedure was executed at least once or an exception informing it that no result was received
				- Can lead to following failures
					- crash failures
					- Arbitrary failures: in cases when the request message is retransmitted
				- At least once can be achieved by retransmission of request message
				- Server may receive it and execute the procedure more than once.
				- Idempotent operation
					- Operation that can be performed repeatedly with the same effect as if it has been performed only once
			- At most once semantics
				- The caller receives either a result, in which case the caller knows the procedure was executed exactly once or an exception informing it that no result was received
				- Ensures that for each RPC a procedure is never executed more than once
		- ## Transparency
			- All the necessary calls to marshalling and message passing were hidden from the programmer
			- Retransmission after a timeout is transparent to the caller
			- Make semantics of RPC like that of local one
			- RPC strives to offer atleast location and access transparency
			- RPC is more vulnerable to failure than local ones
				- involve network, another computer and another process
				- Impossible to distinguish between failure of network and remote server process
				- Client should be able to recover from such situation
			- Latency of RPC much higher than that of local one
	- Implementation of RPC
		- ![image.png](../assets/image_1645272937752_0.png)
		- Client
			- The client that accesses a service includes one stub procedure for each procedure in the service interface
			- This stub procedure behaves like a local procedure
				- but instead of executing the call, it marshals the procedure identifier and arguments into a request message, which it sends to via its communication module to the server
				- It also unmarshals the reply message when it arrives
		- Server
			- Server process contains a dispatcher,
				- one server stub procedure
				- and one service procedure for each procedure in the service interface
			- Dispatcher
				- selects one server stub procedures according to the procedure identifier in the request messsage
			- Server stub procedure
				- It then unmarshals the arguments and calls the corresponding service procedure
				- and marshals the return value
		- RPC is generally implemented over a request reply protocol.
- # Multicast Communication
	- Implementing services as multiple processes in different computers can
		- provide fault tolerance
		- and enhance availability
	- A multicast operation is an operation that
		- sends a single message from one process to each of the members of a group of processes
		- in such a way that the membership of the group is transparent to the sender.
	- ## Multicast communication provides distributed systems with the following characteristics
		- ### Fault tolerance based on replicated services
			- A replicated service consists of a group of servers
			- Client requests multicast communication to all members of the group
			- Even when some members fail, clients can still be served
		- ### Discovering services in spontaneous networking
		- ### Better performance of replicated data
			- replicas of data are placed in user's computers
			- when data changes, a new value is multicast to manage the other replicas
		- ### Propagation of event notification
			- notify processes when something happens
			- publish subscriber protocols makes use of group multicast
	- ## IP Multicast
		- Built on top of the IP protocol
		- Allows use to send IP packets to a set of computers
		- A multicast group uses Class D internet address
		- Membership of a group is dynamic
		- One can send a multicast to a group even if it is not a member
		- IP multicast is available only via UDP
		- IPV4 specific details
			- Multicast routers
				- Multicast both on local network and the internet
			- Multicast address allocation
				- Multicast uses Class D addresses
				- Managed globally by IANA
				- addresses partitioned into blocks
	- ## Failure model for multicast datagrams
		- Similar to UDP failure model since multicast uses UDP
		- Omission failure
			- messages are not guaranteed to be delivered to any group member in the face of a single omission failure
		- Unreliable multicast
			- Some but not all members might receive it
	- JAVA API to IP multicast
		- ??
	- ## Reliability & Ordering of multicast
		- Datagram sent from a multicast router to another may be lost
		- Multicast on a local area network may fail when the buffers of a receiver is full
		- Any process may fail eg: multicast router
		- Ordering is another issue
			- Messages don't arrive in the order they were sent
			- Some group members may receive a different order than other group members
		- ### Effects of reliability & ordering
			- Fault tolerance based on replicated services
				- Either all replicas or none should receive each request to perform an operation
				- if one of them misses a request, then it will become inconsistent wrt other replicas
			- Better performance through replicated data
				- All replicas may not be totally up to date
			- Propagation of event notificaton
				- The particular application determines the qualities required of multicast
- # Group Communication
	- Group communication offers a service where by a message is
		- sent to a group
		- then delivered to all the members of the group
	- The sender is not aware of the identities of the receivers
		- Indirect communication paradigm
	- Group communication represents an abstraction over multicast communication
	- Adds significant extra value in terms of
		- managing group membership
		- detecting failures
		- providing reliability and ordering guarantees
	- Group communication is an important building block in reliable distributed system application areas
		- Dissemination of info to large number of clients
		- Support for collaborative applications
		- support a range of fault tolerance strategies
	- ## Programming Model
		- Central concept of GM
			- process may join or leave the group
		- Implements multicast communication
		- A process issues only one multicast operation to send messages to each of a group of processes
		- The use of a single multicast operation instead of multiple ones is
			- Enables the implementation to be more efficient in its utilization of bandwidth
			- Steps taken to send the message no more than once over any communication link
			- can use network hardware support when it is available
			- Minimizes the time taken to deliver the message to multiple destinations
		- ### Process groups
			- Communicating entities are processes
			- Such services are relatively low level in that
				- Messages are delivered to processes and no further support for dispatching is provided
				- messages are typically unstructured
				- no support for marshalling of complex data types
		- ### Object Groups
			- Collection of objects
			- ??
			- object parameters and results are marshalled
		- ### Open Groups & Closed Groups
			- A group is said to be open if processes outside the group may send to it
			- A group is said to be closed if only members of the group may multicast to it.
		- ### Overlapping & Non Overlapping groups
			- In overlapping entities may be members of multiple groups
	- ## Implementation Issues
		- ### Reliability & Ordering in multicast
			- The guarantee include that
				- every process in the group should receive the set of messages sent
				- delivery ordering across the group members
			- Group communication systems are extremely sophisticated
			- Agreement: If a message is delivered to one process, then it is delivered to all processes in the group
		- Group communication offers ordered multicast with
			- FIFO ordering
			- Casual ordering
			- Total ordering
		- ### Group membership management
			- ??
# Network Virtualization
	- Different classes of applications coexist in the internet
		- eg: peer to peer file sharing and skype
	- It is impractical to alter the internet protocols to suit each
		- IP implemented over a large and ever increasing number of network technologies
	- These two factors have led to Network virtualization
	- Network virtualization is the construction of many different virtual networks over an existing network(such as internet)
		- Each VN designed to support a particular distributed application
	- Overlay Networks
		- a virtual network consisting of nodes and virtual links