public:: true

- ![Module 5 Ktuassist.in.pdf](../assets/Module_5_Ktuassist.in_1654410472711_0.pdf)
- Syllabus
	- RTOS based Design – Basic operating system services. Interrupt handling in RTOS environment. Design Principles. Task scheduling models. How to Choose an RTOS. Case Study – MicroC/OS-II.
- Questions
  collapsed:: true
	- Three processes with process IDs P1, P2, P3 with estimated completion time 6,
	  8, 2 milliseconds respectively, enters the ready queue together in the order.
	  Process P4 with estimated execution completion time 4 milliseconds enters the
	  ready queue after 1 millisecond. (Assuming there is no I/O waiting for the
	  processes) in non- preemptive SJF scheduling algorithm.
	  Calculate the waiting time for each process and average waiting time 19 oct
	- Explain the three methods of ISRs handling in the RTOSs with examples 19 oct\
	- Consider a mobile phone device and look at the main menu. Explain how the
	  events of touching the screen at different points on the screen are handled by an
	  RTOS using two-level ISR handling. 19 may
	- Explain the different types of Inter Task Communication mechanisms
	  supported by MicroC/OS-II kernel. 19 may
	- Three processes with process IDs PI, P2, P3 with estimated completion time 10,
	  5, 7 milliseconds and priorities 0, 3,2 (0—highest priority, 3—lowest priority)
	  respectively enters the ready queue together. Calculate the waiting time and
	  Turn Around Time (TAT) for each process and the Average waiting time and
	  Turn Around Time (Assuming there is no I/O waiting for the processes) in
	  priority based scheduling algorithm 20 sep
	- What are the important functional and non-functional requirements that need to
	  be analysed in the selection of an RTOS for an embedded design? 20 sep
	- Describe the Interrupt Handling mechanism of MicroC/OSII kernel. 20 sep
	- Discuss any two different approaches of Interrupt handling in RTOS
	  Environment with neat diagram and discuss merits and demerits of each.
	  (6)
	  b) Discuss the functional requirements that need to be addressed in the selection
	  of real time operating system. 21 aug
	- Describe any three Task Scheduling models used in RTOS with suitable
	  diagram (6) 21 aug
	- Justify any four design principles of RTOS. 21 aug
	- Differentiate between General purpose OS and Real-Time OS
- Real Time OS is a multitasking OS for the applications with hard or soft real time constraints
- Basic OS functions
	- Process management
	- Resources management
	- Device management
	- I/O device management
- # OS Services
	- ### OS goal
		- OS goals are
			- Perfection
			- Correctness
			- Portability
			- Interoperability
			- Provide common set of interfaces for the system
			- Orderly access and control when managing the processes
		- Easy sharing of resources as per schedule and allocations
		- Easy implementation of application programs with the given system hardware
		- Scheduling of processes
		- Management of processes
		- Common set of interfaces
		- maximizing of system performance
	- ### Processor modes
		- Two modes
			- User mode
			- Supervisory mode
		- User Mode
			- User function call unlike system call, is not permitted to read or write into protected memory allotted to the OS functions, data & heap
		- Supervisor mode
			- OS runs in privileged functions and instructions in protected memory
			- OS only accesses the hardware resources and protected area memory
			- Only a system call is permitted to access protected memory
	- ### System structure
		- Layered model
			- Application Software
			- API
			- System software (other than the OS provided ones)
			- OS Interface
			- OS
			- Hardware OS interface
			- Hardware
	- ### Kernel
		- Kernel is the basic structural unit of any OS in which the memory space of the functions, data and stack are protected from access by any call other than system call
			- It is a secured unit of an OS
		- OS includes
			- A kernel with device management and file management as part of the kernel in the OS
			- A kernel without device management & file management
		- **Functions of kernel**
			- Process management
			- Memory management
			- File management
			- Device management & device drivers
			- I/O sub system management
- # Interrupt Handling in RTOS Environment
	- Interrupt service routines (ISR) are used for interrupt handling
	- ISRs should function as follows
		- ISRs have higher priorities over the OS functions and the application tasks.
		- ISRs does not wait for semaphores, mailbox message or queue message
		- ISRs also does not wait for mutex
	- ## Systems for OS to respond to hardware source calls from the interrupts
		- ### Direct call to ISR by an interrupting source
			- ![image.png](../assets/image_1654501235213_0.png){:height 279, :width 479}
			- Interrupt source directly calls the ISR
			- ISR then sends a message to the RTOS
			- On interrupt the process running at the CPU is interrupted and the ISR corresponding to that source starts executing
			- When ISR finishes, RTOS returns to the interrupted process or reschedules the process
		- ### RTOS first interrupting on an interrupt, then OS calling the corresponding ISR
			- ![image.png](../assets/image_1654501541614_0.png){:height 281, :width 514}
			- Interrupt sources comes to the OS.
			- The OS will then save the context of the current task.
			- Then it will call the corresponding ISR
		- ## RTOS first interrupting on an interrupt, then RTOS initiating the ISR and then an ISR
			- ![image.png](../assets/image_1654501594842_0.png){:height 259, :width 465}
			- RTOS can provide two levels of ISRs
				- Fast level ISR
					- Hardware interrupt ISR
					- Reduced interrupt latency
				- Slow level ISR
					- Software interrupt ISR
					- Called as interrupt service thread in Windows CE
					- IST functions as a deferred procedure call (DPC) of the ISR
			- RTOS first interrupted on an interrupt
				- then OS calling a fast ISR
				- and then fast ISR calls a slow ISR (IST)
			- Hardware interrupt takes time. So in fast ISR we try to collect the necessary information from the hardware with low latency and pass it to the IST
- # RTOS Design Principles
	- An embedded system with one cpu can run only one process at an instance
	- The process at any instance may either be an ISR, kernel function or task.
	- RTOS in a embedded system
		- An RTOS provides running the user thread in kernel space so that they execute fast
		- It provides effective handling of the ISRs, device drivers, ISTs, tasks or threads
		- It provides memory allocation and de-allocation function in fixed time
		- It provides effectively scheduling and running and blocking of the tasks in case of many tasks
		- IO management with devices, files, mailboxes, pipes and sockets becomes simple using an RTOS
		- Effective management of multiple states of the CPU
	- ## Design Principles
		- Design with ISRs and tasks
		- Design with using interrupt service threads or interrupt service tasks
		- Design each task with an infinite loop from start to finish
		- Use idle CPU time for internal functions
		- Avoid task deletion
- # Task Scheduling in RTOS
  collapsed:: true
	- Task scheduling is required for multitasking
	- Sharing CPU among different tasks
	- Which process to be executed at a point of time
	- Scheduling is done by scheduler
	- Process states
		- Ready
		- Running
		- Blocked / waiting
		- Completed
	- Non Preemtive
		- FCFS
		- LCFS
		- SJF
		- Priority based
	- Preemptive
		- Preemptive SJF or Shortest Remaining Time (SRT)
		- Round Robin scheduling
		- Priority Based Scheduling
	- Common models that a scheduler may adopt
		- ## Control flow strategy
			- Complete control of sequencing of the inputs and outputs
			- Control steps are deterministic
			- Worst case interrupt latencies are well defined
			- Scheduler programs such that its states are predetermined by the programming codes
			- Cooperative scheduler adopts this strategy
		- ## Data flow strategy
			- Interrupt occurrences and task control steps may not be deterministic
			- The sequencing of inputs and outputs is not feasible
			- A preemptive scheduler adopts the data flow strategy
			- The data generated from the output function at transition determines the next sequence in the program
		- ## Control data flow strategy
			- A task design and scheduler functions may provide for the time out delay features with deterministic delays
	- Task scheduling models
		- ### Cooperative Scheduling Model
			- Each task cooperate to let the running task finish
			- No task blocks the running task between the ready to finish states
			- Serves in cyclic order
		- ### Cooperative with precedence constraints
- # How to Choose an RTOS
	- From MEC pdf
	- Decision of an RTOS for embedded design is very critical
	- Factors that need to be analysed
		- Functional Requirements
			- Processor support
			- Memory requirement
			- Real time capabilities
			- Kernel & interrupt latency
			- Task synchronization & IPC
			- Support for networking and communication
			- Development Language support
		- Non Functional Requirements
			- Custom made or off the shelf
			- Cost
			- Development and debugging tool availability
			- Ease of use
			- After sales