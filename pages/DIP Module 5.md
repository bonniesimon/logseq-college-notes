- # Segmentation
	- Segmentation subdivides an image into constituent regions or objects
	- It is done to locate objects and boundaries (lines, curves, etc) in images
	- It mainly depends on two values
		- Discontinuity
			- partition the image based on abrupt changes in intensity values such as edges in images
		- Similarity
			- Partition edges into regions that are similar according to predefined criteria
	- Basic procedure to detect discontinuity is to run a mask and compute the sum of products of the coefficients with the gray level encompassed by the mask
	- ## Point detection
		- Mask is
		  ![image.png](../assets/image_1646118438075_0.png)
		- R = w1z1 + w2z2 + ...... + wnzn
		- If R >= T,  then it is a point
		- Isolated point will be quite different from its surrounding, thus easily detectable by such a mask
		- Mask response will be zero in areas with constant intensity
	- ## Line Detection
		- ![image.png](../assets/image_1646118537526_0.png)
		- if |Ri| > |Rj|, then the point will more likely be associated with a line in the direction of mask i
	- ## Edge Detection
		- commonly used approach for detecting discontinuity
		- Edge is the boundary between two regions having distinct intensity values
		- Uses first order and second order derivative for detection of edges.
		- ![image.png](../assets/image_1646119277911_0.png)
		- The first derivative is positive at the point of transition into and out of the ramp as we move from left to right
			- constant for points in the ramp and zero in areas of constant grey level
		- The second derivative is positive at the transition associated with the darker side and negative at the transition associated with the lighter side and zero along the ramp
			- Sensitive to noise
		- Sign of second derivative is used to detect whether a point is on the darker side or brighter side.
		- Edge detection notes in DIP M5 ktunotes