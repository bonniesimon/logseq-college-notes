file-path:: ../assets/Module5_DMW_1654783866117_0.pdf
file:: [Module5_DMW_1654783866117_0.pdf](../assets/Module5_DMW_1654783866117_0.pdf)
title:: hls__Module5_DMW_1654783866117_0

- lustering is the process of grouping the data into classes or clusters so that objects withina cluster have high similarity in comparison to one another, but are very dissimilar to objects in other clusters.
  ls-type:: annotation
  hl-page:: 14
  id:: 62b1919f-13e3-47e2-b882-dc2768a1e1af
- In  business,  clustering  may  help  marketers  discover  distinct  groups  in  their  customer  bases  and characterizecustomer groups based on purchasing patterns
  ls-type:: annotation
  hl-page:: 14
  id:: 62b191f2-5458-4a41-b1bb-c3c128f2647d
- As a data mining function, cluster analysis can be used as a stand-alone tool togain insight into the distribution of data, to observe the characteristics of each cluster, and to focus on a particularset of clusters for further analysis.
  ls-type:: annotation
  hl-page:: 14
  id:: 62b19224-22a8-453d-8465-e3620e3f045f
- The  k-means  algorithm  is  sensitive  to  outliers  since  an  object  with  an  extremely  large  value  may substantially distortthe distribution of data.
  ls-type:: annotation
  hl-page:: 21
  id:: 62b1db02-7b7a-42a8-9ade-4f392beb0766