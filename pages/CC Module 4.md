public:: true

- ![Module 4 by Miss.pdf](../assets/Module_4_by_Miss_1653850155365_0.pdf)
- Syllabus
	- Parallel Computing and Programming Paradigms – Map Reduce – Twister – Iterative Map Reduce – Hadoop Library from Apache – Pig Latin High Level Languages- Mapping Applications to Parallel and Distributed Systems – Programming the Google App Engine – Google File System (GFS) – Big Table – Google’s NOSQL System
- # Parallel Computing & Programming Paradigms
	- ## Issues
		- Partitioning
		- Computation partitioning
			- Splits a program into smaller tasks
			- Difficulty in identifying parts of a program that can be done concurrently
		- Data partitioning
			- Splits input or intermediate data into smaller parts
		- Mapping
			- It assigns smaller parts of the program or smaller parts of the data to underlying resources
		- Synchronization
			- Since different workers perform different tasks, synchronization & coordination among workers is necessary so that race conditions are prevented and data dependency among different workers is properly managed
		- Communication
			- Data dependency is a main reason for communication among workers
			- communication is triggered when data is sent to workers
		- Scheduling
			- When the number of computation parts or data parts is more than the number of available workers, a scheduler is needed to select the tasks that are to be assigned to workers
	- ## Motivation for Programming Paradigms
		- Handling the whole data flow of distributed and parallel programming is a very time consuming task
			- it also requires special knowledge of programming
		- Thus it reduces programmer productivity
			- It also leads to an increased program time to market
			- It detracts the programmer from focussing on the logic of the program itself
		- So models or paradigms that abstract the data flow of distributed and parallel programming is required
		- Simplicity of writing parallel programs is a metric for distributed and parallel programming paradigms
- # MapReduce
	- It is a framework which support parallel and distributed computing on large data sets
	- Formal definition
		- It is a software framework that provides an abstraction layer with the data flow and the flow of control to users
			- and hides the implementation details of all data flow steps such as partitioning, mapping, synchronization, communication & scheduling.
	- The abstraction layer provides two interfaces: Map & Reduce
		- These two functions can be overridden by the user to achieve specific objectives
	- ![image.png](../assets/image_1654667821316_0.png)
	- The user first overrides the Map and Reduce functions
		- Then he invokes the provided `MapReduce(Spec, &Results)` to start the flow of data
	- `MapReduce` function takes an important parameter which is a specification object, `Spec`
		- This object is first initialized in the user's program.
		- The input and output files are specified inside this object, along with other optional tuning parameters
		- It also consists of the name of the `Map` & `Reduce` user defined functions
	- ```
	  Map Function (... . ) 
	  {
	  	... 
	      ... 
	  }
	  
	  Reduce Function (... . ) 
	  {
	  	... 
	      ... 
	  }
	  
	  Main Function (... . ) 
	  {
	  	Initialize Spec object 
	      ... 
	      ... 
	      MapReduce (Spec, & Results)
	  }
	  ```
	- ![image.png](../assets/image_1654668146084_0.png)
	- ## Formal notation of MapReduce data flow
		- Map is applied in parallel to every input (key value ) pari and produces a new set of intermediate (key, value) pair
			- ![image.png](../assets/image_1654668253194_0.png)
		- Then MapReduce collects all the produced key value pairs from all input (key, value) pairs and sorts them based on the "key" part
			- It then groups the values of all occurrences of the same key
		- Finally the Reduce function is applied to each group producing the collection of values as output
			- ![image.png](../assets/image_1654668358307_0.png)
	- ## Strategy to solve MapReduce Problems
		- ??
- # Twister & Iterative MapReduce
	- ??
- # Pig Latin
	- Merge two student files
	  ```
	  student1 = LOAD 'hdfs://localhost:9000/pig_data/student_data1.txt' USING PigStorage(',') 
	     as (id:int, firstname:chararray, lastname:chararray, phone:chararray, city:chararray); 
	   
	  student2 = LOAD 'hdfs://localhost:9000/pig_data/student_data2.txt' USING PigStorage(',') 
	     as (id:int, firstname:chararray, lastname:chararray, phone:chararray, city:chararray);
	     
	  student = UNION student1, student2;
	  Dump student; 
	  ```
	- Order student file by name
	  ```
	  student_details = LOAD 'hdfs://localhost:9000/pig_data/student_details.txt' USING PigStorage(',')
	     as (rollno:int, firstname:chararray, lastname:chararray,age:int, phone:chararray, city:chararray);
	  order_by_data = ORDER student_details BY rollno DESC;
	  Dump order_by_data; 
	  ```
- # Hadoop by Apache
	- Open source implementation of MapReduce
		- coded and released in Java rather than C
		- by Apache
	- It uses Hadoop distributed file system (HDFS) as its underlying layer rather than GFS
	- Hadoop core is divided into two layers:
		- MapReduce engine
		- HDFS
	- MapReduce engine is the computational engine running on top of HDFS
	- HDFS is a distributed file system that organizes files and stores their data on a distributed computing system
		- inspired by GFS
	- HDFS architecture
		- master slave architecture
		- NameNode as master
		- DataNodes as workers (slaves)
		- A file is split into 64 MB sized blocks and stored on workers (DataNodes)
		- Mapping of blocks to DataNodes is determined by NameNodes
		- NameNode also manages the file system's metadata & namespace
	- Two important characteristics of HDFS to distinguish it from generic distributed file systems:
		- Fault tolerance
		- High throughput access to large data sets
	- HDFS Fault tolerance
		- It is one of the distinguishing characteristics of HDFS
		- It is meant to run on low cost hardware
			- so hardware failures are considered common rather than an exception
		- To ensure reliability, HDFS has the following requirements
			- Block Replication
				- Blocks are replicated in the system
				- Each block stored in HDFS is replicated and distributed across the cluster
				- The replication factor is set by the user or 3 by default
			- Replica placement
				- Storing replicas on different DataNodes located in different racks across the whole cluster provides more reliability
				- Cost of communication between two nodes in different racks in relatively high that different nodes in the same rack
				- Sometimes HDFS compromises on reliability for lower communication costs
				- Default replication factor is 3
					- one replica on the same node,
					- one on a different node on the same rack,
					- one on a different node in a different rack
			- Heartbeat and Block report messages
				- They are periodic messages sent to the NameNode by each DataNode in a cluster
				- receival of heartbeat => DataNode is functioning properly
				- receival of block report => it contains the list of all blocks on a DataNode
	- HDFS High throughput access to large data sets
		- Another distinguishing characteristic of HDFS
		- Because HDFS is used for batch processsing rather than interactive processing
			- having high access throughput is more important than latency
	- ## HDFS Operation
		- Reading a file
			- An open request is sent to the NameNode to get the location of the file blocks
			- For each file block, the NameNode returns the address of a set of DataNodes containing the replica information of the requested file
			- The user on getting such information calls the read function to connect to the nearest Datanode containing the first block of the file
		- Writing to a file
			- A "create" request is sent to NameNode by the user
				- to create a new file in the FS namespace
			- If file doesn't exist, then NameNode notifies user and allows him to write data to the file using write function
			- First block of the file is written to an internal queue (data queue)
				- while data streamer monitors its writing into DataNode
			- Based on the replication factor, the streamer request DataNodes to store replica to the NameNode
			- Streamer then stores the block in the first allocated DataNode
			- Afterwards, the first DataNode forwards it to the second DataNode.
			- This process continues until all allocated DataNodes receive a replica
			- Once this is over, the same process repeats for the second block of data.
	- ## Architecture of MapReduce in Hadoop
		- Top most layer of Hadoop is the MapReduce engine
		- It manages the data flow and the control flow of the MapReduce jobs over the distributed systems
		- ![image.png](../assets/image_1655359652251_0.png)
		- MapReduce Architecture
			- Similar to HDFS
			- It has a master slave architecture
			- JobTracker is the master and TaskTracker is the slave
			- It consists of a single JobTracker and multiple TaskTrackers
			- JobTracker
				- manages the MapReduce job over a cluster
				- responsible for monitoring jobs and
				- assigning tasks to TaskTracker
			- TaskTracker
				- manages the execution of the map and / or reduce tasks on a single computation node in the cluster
	- ## Running a Job in Hadoop
		- 3 components contribute to running a job in this system
			- user node
			- Job tracker
			- TaskTracker
		- ![image.png](../assets/image_1655360067381_0.png)
		- `runJob(conf)` function is called in the user program running on the user node
			- This starts the data flow
			- `conf` contains tuning parameters for MapReduce and HDFS
		- ### Job Submission
			- Each job is submitted from a userNode to the JobTracker node
			- userNode asks for a JobID from the JobTracker and computes the input file split
			- user node copies resources such as config file, input file split, etc to the JobTracker's file system
			- user node submits the job to the JobTracker
		- ### Task Assignment
			- JobTracker creates a map task for each input file split
			- and assigns it to the execution slots in TaskTracker
			- Reduce task is predetermined by the user
		- ### Task Execution
			- control flow to execute a task starts
			- ??
		- ### Task running check
			- Task running check is performed by receiving a heartbeat message from the TaskTracker to the JobTracker
- # Programming the Google App Engine
	- ??
- # Google File System (GFS)
	- Built as the fundamental storage service for googles search engine
	- As size of data increased, google needed a distributed file system
	- It was developed for google apps and google apps were built for GFS
	- Assumptions made
		- As servers are composed of inexpensive commodity components, concurrent failures will occur all the time
		- File size in GFS:
			- It will hold large number of huge files, each of 100MB or larger
			- Google has chosen 64MB as it file data block size rather than 4 KB
		- IO pattern
			- files are written once and write operations are often appending data blocks to end of files
			- Multiple appending operations might be concurrent
	- Reliability is achieved by using replication
		- Each data block of a file is replicated across more than 3 chunk servers
		- A single master coordinates access as well as keeps the metadata
	- Developers need not worry about different issues in the distributed computing
	- ## Architecture of GFS
		- There is a single master for the whole cluster
		- Other nodes act as chunk servers
			- for storing data
		- Master stores the metadata,
			- manages file system namespace and
			- locking facilities
		- Master periodically communicates with the chunk servers to collect management information
			- also to give instructions to the chunk servers to do work such as load balancing and fail recovery
		- Master has enough information to keep the whole cluster in a healthy state
		- Single master can handle as much as 1000 nodes
		- Disadvantage
			- Master is a single point of failure and bottleneck
			- Solution: Use shadow master to replicate all the data on the master
			- Control messages are transferred between the master and client and they can be cached for future use
- # BigTable - Google's NoSQL System
	- It was created for storing and retrieving structured and semi structured data
	- BigTable applications include web pages, per-user data and geographic locations
	- Web pages are used to represent URLs, and their associated data
		- Per user data for specific user includes user preference settings, recent queries/search results
		- Geographic locations include physical entities, roads, satellite image data
	- Storing such large scale of structured or unstructured data is not possible using commercial database system
	- Motivation for building a new data management system
		- New system can be applied over different projects for a low incremental cost
		- Low level storage optimizations can lead to increased performance which is much harder when running on top of a traditional database layer
	- Design goals
		- Applications will have asynchronous processes that will continuously be updating different pieces of data and want access to the most current data at all times
		- It needs to support very high read / write rates and scale might be millions of operations per second
		- It needs to support efficient scans over all or interesting subsets of data
		- Also efficient joins
		- It should examine data changes over time
	- BigTable can be viewed as a distributed multilevel map
	- BigTable features
		- It provides a fault tolerant and persistent database
		- It is scalable
		- It is a self managing system
			- servers can be added or removed dynamically and also has auto load balancing
	- BigTable is built on top of other google cloud infrastructure
		- MapReduce
		- GFS
		- Scheduler
		- Lock service
	- ## BigTable data model
		- We'll be looking on the data model of a sample table called the web table. It stores the data about a web page
		- URL of the web page is considered the row index.
			- Column provides different data related to corresponding URL. Eg: different versions of the contents & the anchors appearing in the web page
			- ![image.png](../assets/image_1654666917667_0.png)
		- The Map is indexed by row key, column key and timestamp
			- i.e. (row: string, column: string, time: int64) maps to string (cell contents)
		- Similar to GFS, a master machine makes the load-balancing decisions
			- The master manages and stores the metadata of the BigTable system
		- Clients communicate with the master and tablet servers
		- ![image.png](../assets/image_1654667075031_0.png){:height 345, :width 628}
	- Tablet Location Hierarchy
		- ??