public:: true

- ![Module 4 Lambda Calculus.pdf](../assets/Module_4_Lambda_Calculus_1639840025770_0.pdf)
- ![Module 4 Functional programming concepts.pdf](../assets/Module_4_Functional_programming_concepts_1639840034469_0.pdf)
- ![Module 4 IO Streams Monads.pdf](../assets/Module_4_IO_Streams_Monads_1639840044750_0.pdf)
- ![Module 4 Higher Order Functions.pdf](../assets/Module_4_Higher_Order_Functions_1639840058503_0.pdf)
- ![Module 4 Logic Programming.pdf](../assets/Module_4_Logic_Programming_1639840065152_0.pdf)
- [Another Note](https://drive.google.com/file/d/1hPkpHAl_n-ci9QG0saf37z0-a-gP3xer/view)
- Syllabus
	- Functional and Logic Languages:- Lambda Calculus, Overview of Scheme, Strictness and Lazy Evaluation, Streams and Monads, Higher-Order Functions, Logic Programming in Prolog, Limitations of Logic Programming
- # Lambda Calculus
	- Lambda calculus is a framework developed to study computations with functions.
	- Bound variable: a variable that is associated with some lambda (watch [this](https://youtu.be/Paxvaq0Q-S0?list=TLPQMjgwMTIwMjKuofCuGl9TUg))
		- (Lambda (x) (x y))
		- Here x is bound since it is a parameter for lambda function and y is free variable (not exact definition but idea is same)
	- Free variable: a variable that is not associated with any lambda
# Functional Programming Concepts
	- Defines output of a program as a function of input
	- Functional programming defines the output is a mathematical function of the inputs, with no notion of internal state, and thus no side effects.
	- Recursion is the preferred means of repetition.
	- Miranda, haskell, PH, single assignment C
	- ![image.png](../assets/image_1643432852449_0.png)
	- Features
		- First class functions & higher order functions
			- can be passed as parameter, returned from function or assigned into a variable
		- Extensive polymorphism
		- Supports list types and operators
		- Structured function returns
		- constructors for structured objects
		- Garbage collection
	- ## Scheme
		- Dialect of Lisp
		- Small size
		- Use of static scoping
		- Function is a first class utility
		- Type is not determined at runtime
		- Some syntaxes
			- (lambda (params..) body)
			- ((lambda (params) body) arguments_passed)
			- (define symbol expression)
			- (define (function_name parameters) (expression))
			- (define function_name (lambda (params) (lambda_body) ))
			- (if predicate then_expression else_expression)
			- (cond ((cond1) body) ((cond2) body) (else body))
				- ```scheme
				  (define (compare x y)
				    (cond
				     ((> x y) "x is greater than y")
				     ((< x y) "y is greater than x")
				     (else "Both are equal")
				    )
				   )
				  ```
		- Let
			- [watch this](https://youtu.be/y25-KYfviEw)
			- Names can be bound to values by introducing a nested scope
			- "let" creates a new local static scope
			- syntax
				- ```scheme
				  (let
				  	(
				       	(varaible value)  
				       	(varaible2 value)
				       	(fn_name (lambda (params) expression)
				      )
				      (expression expression) 
				  )
				  ```
			- Let evaluates all bindings with respect to the level above it
				- ```scheme
				  (let
				  	(
				       	(x 3)
				       	(y (+ x 1)) ;;; here x will be taken from the outer scope
				      )
				    	(+ x y)
				  )
				  ```
				- In the above let construct, since there is no x in the outer scope, it will return an error
				- ```scheme
				  (define x 20)
				  (let
				  	(
				      	(x 3)
				       	(y x)
				      )  
				    	(+ x y) ;;; => output will be x+y = 3 + 20 = 23
				  )
				  ```
				- Since let takes values from the outer scope, in (y x) y takes the value of x in the outer scope, which is 20
				- Consider
				  ```scheme
				  (let
				  	(
				      	(x 3) 
				      ) 
				    	(let 
				      	(
				          	(y (+ x 1)) ;;; Here x comes from the outer scope, so x = 3 & y = 4
				          ) 
				        	(+ x y) ; output is x + y = 3 + 4 = 7
				  	)
				  )
				  ```
				- Here the outer scope in (y ( + x 1)) is the outer let construct
				- ```scheme
				  (let
				  	(
				      	(a 3)
				       	(b 4)
				       	(square (lambda (x) (* x x)))
				       	(plus +)
				      )  
				    	(sqrt (plus (square a) (square b))) ; output is 5.0
				  )
				  ```
		- let*
			- let* evaluates all bindings sequentially
			- ```scheme
			  (let
			  	(
			      	(a 3)
			      )
			    	(let* 
			      	(
			          	(a 4) 
			           	(b a) ; since let*, b takes the value of a that comes before it, so b = 4
			          )  
			      	(+ a b) ;output is 8 (4 + 4)
			      )
			  )
			  ```
		- letrec
			- ```scheme
			  (let
			  	(
			      	(fact (lambda (n) 
			          	(if (= n 1) 1 (* n (fact(- n 1)) ))        
			  		)) 
			      )
			    	(fact 5) ; error fact is undefined
			  )
			  
			  ; so we use letrec
			  
			  (letrec
			  	(
			      	(fact (lambda (n) 
			          	(if (= n 1) 1 (* n (fact(- n 1)) ))        
			  		)) 
			      )
			    	(fact 5) ; => 120
			  )
			  ```
-