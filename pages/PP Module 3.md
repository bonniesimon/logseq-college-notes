public:: true

- ![Module 3 by Miss.pdf](../assets/Module_3_by_Miss_1639408444872_0.pdf)
### Doubts
	- ((61b832f8-f8fd-4437-966d-32079da41553))
	- ((61b83fdd-7bb0-4a6d-9bd4-2c9e00832f2c))
	- ((61b83f0b-d51c-4476-93bb-8ea2dd0ac6ee))
- Static Links
	- Each stack frame contains a reference to the frame of lexically surrounding subroutine
	- This reference is static link
- Dynamic Link
	- The saved value of the frame pointer, ,which will be restored on subroutine return is called dynamic link
- Maintaining the Static chain
	- part of the work to maintain the static chain must be done by the caller
	- If callee is nested inside the caller, then the callee static link should refer the caller's frame
	- If the callee is on the same level as of nesting as the caller, then the callee's static link is a copy of the caller's static link
	- If the callee is k levels higher than the caller
		- then callee must follow k static links back from the callee to the callers static link
		- then copy static link found here
- # Calling Sequences
	- Maintaining the subroutine call stack is the responsibility of the calling sequence
		- The code executed by the caller immediately before and after a sub routine call
		- And the prologue (code executed at the beginning) & epilogue of the subroutine itself
			- Prologue - prepares stack & registers for use
			- Epilogue - Restores stack & registers
	- Combined operations of the caller, prologue & epilogue is referred to as the calling sequence
	- The caller
		- saves any caller-saves registers whose value will be needed after the call
		- it computes the value of the arguments and moves them onto stacks or registers
		- computes the static link and passes it on as extra argument
		- executes a special subroutine jump instruction and passes the return address on the stack or in a register
	- In prologue, the callee
		- allocates frame by subtracting an appropriate constant from the sp
		- saves the old stack pointer onto the stack, & assigns it a new value
		- saves any caller-saves registers that may be overwritten
	- The epilouge
		- moves the return address into a register
		- restores callee-saves registers if needed
		- restores fp and sp
		- jumps back to the caller
	- Finally the caller
		- moves the return value to where ever it is needed
		- restores caller-saves registers if needed
	- Displays
		- The problem with static chains is that to access an object in scope k levels out requires the static chain be dereferenced k times
		- This number can be reduced to a constant by using displays
		- A display is an array that replaces the static chain
			- the jth element of the display contains the reference to the most recent active subroutine
		- An object k levels out can be found by using j = i-k
			- j - static link to be found
			- i - frame number of subroutine
			- k - no of nesting levels
	- Register Windows
		- RISC machines use Register Windows as an alternate to saving and restoring registers on subroutine calls
		- Register file is divided into register windows
		- Each register window is assigned to a procedure
		- Each window divided into 3:
			- Parameter registers
			- Local registers
			- Temporary registers
	- In Line Expansion
		- alternate to stack based calling convections
		- subroutines are expanded in line at the point of call
		- A copy of the "called" routine becomes part of the "caller"
		- Avoids overheads like
			- space allocation
			- branch delays from call and return
			- maintaining static chain
			- saving and restoring registers
- # Special Purpose Parameters
	- Conformant arrays
		- Size is not known till runtime
	- Default or optional parameters
		- this parameter need not be given by the caller
		- if the value is not passed / missing then the default value is used
	- Named Parameters
		- Parameters are positional. First actual parameter corresponds to the first formal parameter
		- But in Ada, common Lisp, fortran 90, modula -3 & python this need not be the case
		- Since parameters are named, their order doesn't matter.
		- positional arguments can't follow named arguments
			- but named arguments can follow positional arguments
	- Variable number of arguments
		- Lisp, python, C and its descendants allow subroutines that take a variable number of arguments
		- The ellipsis (...) in the function header denote additional parameters following the format
		- ```
		  int printf(char *format, ...)
		  {.......}
		  ```
		- Steps to use variable arg list in c/c++
		  id:: 61b832f8-f8fd-4437-966d-32079da41553
			- ??
- # Function Returns
	- In Lisp, ML & Algol 68 the value of the function is simply the value of its body, which is itself an expression
	- Return marks the termination of a function
	- If we have the return value before the function ends, we can store it in a variable and return it later
		- ```
		  rtn = expression
		  ...
		  Return rtn
		  ```
		- But this introduces some problems
			- since rtn is a local variable, compilers have to allocate it within the stack frame of the function
			- The return statement must then perform an unnecessary copy to move that variable's value into the return location allocated by the caller
	- Some languages eliminate this need for a local variable
		- by allowing the result of a function to have a name in its own right
		- In SR, the value stored in `rtn` is implicitly returned at the end of a function
		- No local copy is kept
	- Multivalue Returns - Python
		- Python allows multivalue returns
- # Parameter passing Modes
	- formal parameters : parameters in the subroutine declaration
	- actual parameters : parameters in the subroutine call (aka arguments)
	- Call by Value & Call by Reference
	- Call by value / Result
		- Like call by value, it copies the actual parameter into the formal parameter at the beginning of the subroutine execution.
		- Unlike call by value, it also copies the formal parameters back to the actual parameters when the subroutine returns.
	- Call by sharing
		- We pass the reference and let the actual and formal parameters share the same object
		- Caller shares the object with the callee
		- Callee mutates the shared object as long as the object is mutable
		- Since object is shared, any changes made by the callee is reflected in the caller
		- For immutable object, it is similar to call by value
	- Read only parameters
		- Available in Modula3
		- Formal parameters that are declared read only cannot be changed by the called routine
		- similar to const in C
	- Pascal
		- call by value default
		- call by reference is used if the variable is preceded by var
	- C
		- call by value default
	- Fortran
		- uses call by reference
		- no call by value support
	- Parameter modes in Ada
		- 3 modes
			- in, out, in out
		- In
			- callee can read parameters passed, by cannot write them
		- Out
			- Parameters passed can be written by callee but not read
		- In out
			- Can be read and write
		- Changes to out or in out parameters always change the actual parameters
		- Implementation
			- in - call by value
			- out - call by result
			- in out - call by value / result
	- References in C++
		- C lacks reference variables
			- on can pass the address of an object by then the formal parameter is a pointer and needs to be explicitly dereferenced (using *)
		- C++ solves this problem by using reference
			- ```
			  int i;
			  int &j = i;
			  ...
			  i = 2;
			  j = 3;
			  cout << i; // prints 3
			  ```
			- No dereferencing required
			- ```
			  void swap(int &a, int &b) { intt=a;a=b;b=t;}
			  ```
		- Referenced parameters are specified by an '&' preceding their name in the header of the function
	- Closures as Parameters
	  id:: 61b83f0b-d51c-4476-93bb-8ea2dd0ac6ee
		- ??
	- Call by Name (Algol60 & simula)
		- Re evaluates actual parameters on every use
		- Arguments that are simple variables : same as call by reference
		- Arguments that are expression : re-evaluated on each access
		- Call by Need (Miranda, Haskel, R)
			- Memorized version of call by name
		- ? example is not clear ((61b83fe9-4d00-456e-a459-8f61686a71ad))
		  id:: 61b83fdd-7bb0-4a6d-9bd4-2c9e00832f2c
	-
# Generic Subroutines & Modules
	- In large programs, we might need to do operations on many different types
	- For eg, os requires queues. But there are different types that a queue can handle. Making enqueue and dequeue operations for each type is difficult.
		- Characteristics of a queue are independent of the type of objects placed inside the queue.
	- Implicit Parametric polymorphism
		- Provides a solution
		- allows us to declare subroutines with parameter types by incompletely specified
			- delays type checking until run time
			- makes compiler slower and complicated
	- Explicit parametric polymorphism
		- allows a collection of similar subroutines or modules
			- with different types
			- to be created from a single copy of the source code
		- Languages that provide generics include Ada, C++ (which calls them templates), Clu, Eiffel, Modula-3, Java, and C#.
	- Generic parameters
		- Java & Csharp allow to pass generic types
		- Ada & C++ allows ordinary types
	- Generics in C++ are called Templates (view text for eg, pdf pg 444)
	  ```cpp
	  template<class item, int max_items = 100> 
	  class queue { 
	  	item items[max_items]; 
	  	int next_free; 
	  	int next_full;
	  	public: 
	      queue() { 
	      	next_free = next_full = 0; 
	      }
	  	void enqueue(item it) { 
	      	items[next_free] = it; 
	      	next_free = (next_free + 1) % max_items;
	    	}
	  	item dequeue() { 
	      	item rtn = items[next_full]; 
	      	next_full = (next_full + 1) % max_items; 
	      	return rtn;
	  	} 
	  }; 
	  
	  ...
	  
	  queue<process> ready_list; 
	  queue<int, 50> int_queue;
	  ```
	- Implementation Options
		- Ada, C++ use static mechanism
		- All the work required to create and use multiple instances of the generic code takes place at compile time
		- Compiler creates a separate copy of the code for every instance
	- ??
# Coroutines
	-