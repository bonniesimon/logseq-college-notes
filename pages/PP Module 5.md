public:: true

- ![Module 5 by Miss.pdf](../assets/Module_5_by_Miss_1642741793286_0.pdf)
- ![Module 5 (ktustudents.in).pdf](../assets/Module_5_(ktustudents.in)_1644686329628_0.pdf)
- Syllabus
	- Data Abstraction and Object Orientation:-Encapsulation, Inheritance, Constructors and Destructors, Aliasing, Overloading, Polymorphism, Dynamic Method Binding, Multiple Inheritance. Innovative features of Scripting Languages:-Scoping rules, String and Pattern Manipulation, Data Types, Object Orientation.
- # Object Oriented Programming
	- Fundamental concepts in OOP
	  id:: 6207ec06-07eb-48d8-8a1b-3c516950ceaa
		- Encapsulation
		- Inheritance
		- Dynamic method binding
	- Polymorphism
		- Provides one common interface for many implementations through dynamic method binding
	- An Abstract Data Type is one that is defined in terms of the operation that it supports (i.e. that can be performed upon it) rather than in terms of its structure or implementation
	- Features of OOP
		- {{embed ((6207ec06-07eb-48d8-8a1b-3c516950ceaa))}}
		- Enables code reuse. Making it easier to define abstractions as extensions to existing abstractions
		- Classes have data members and subroutine members (Member functions)
		- Constructors - creation of objects
		- Destructors - destruction of objects - used for storage management and error checking.
		- Access specifiers - public, private, protected.
		- Inheritance
		- constructor overloading.
	- ## Inheritance
		- Inheritance helps in code reuse
		- A class can be declared as the subclass of another class which is called the super class or parent class.
		- ### Multiple inheritance
			- It is useful to derive features from more than one base class.
			- The problem occurs when there exist methods with the same signature in both the superclasses and subclasses.
			- On calling the method, the compiler cannot determine which class method to be called and even on calling which class method to give priority
			- It cause problems during various operations like casting, constructor chaining, etc.
			- #### Java
				- Java handles this problem by using default methods and interfaces.
				- A class can implement two or more interfaces.
				- Default methods allows an interface to provide a default implementation of methods
				- The interfaces that are going to be extended from will have methods with same signature, but the implementing class should explicitly specify which default method to be used or it should override the default method
				- ```java
				  // Java program to demonstrate Multiple Inheritance
				  // through default methods
				  
				  // Interface 1
				  interface PI1 {
				  
				  	// Default method
				  	default void show()
				  	{
				  
				  		// Print statement if method is called
				  		// from interface 1
				  		System.out.println("Default PI1");
				  	}
				  }
				  
				  // Interface 2
				  interface PI2 {
				  
				  	// Default method
				  	default void show()
				  	{
				  
				  		// Print statement if method is called
				  		// from interface 2
				  		System.out.println("Default PI2");
				  	}
				  }
				  
				  // Main class
				  // Implementation class code
				  class TestClass implements PI1, PI2 {
				  
				  	// Overriding default show method
				  	public void show()
				  	{
				  
				  		// Using super keyword to call the show
				  		// method of PI1 interface
				  		PI1.super.show();
				  
				  		// Using super keyword to call the show
				  		// method of PI2 interface
				  		PI2.super.show();
				  	}
				  
				  	// Mai driver method
				  	public static void main(String args[])
				  	{
				  
				  		// Creating object of this class in main() method
				  		TestClass d = new TestClass();
				  		d.show();
				  	}
				  }
				  
				  ```
	- ## Encapsulation
		- Encapsulation is the property of grouping data and the functions that act on the data in one place and to hide irrelevant information from the users
		- Eg: procedures, packages, classes
		- By using encapsulation the program can limit the visibility of the data values
		- ### Visibility Rules
			- Any class can limit the visibility of its members
			- A derived class can restrict the visibility of members of a base class but can never increase it.
			- C++ has 3 access specifiers
				- Public class members
					- accessible to anybody
				- Protected class members
					- accessible to members of this or derived class
					- It is visible only to methods of its own class or of classes derived from that class
					- protected keyword can be used when specifying base class
						- ```cpp
						  class derived : protected base{...}
						  ```
					- Here public members of base act like protected members of derived class
				- Private class members
					- accessible for just the members of this class
			- C++ struct is simply a class with all its members public
			- C++ base classes can also be public, protected or private
			- Syntax of inheritance
				- C++
					- ```cpp
					  class push_button : public widget{...}
					  ```
				- Java
					- ```java
					  public class push_button extends widget{...}
					  ```
				- Ada
					- ```ada
					  type push_button is new widget with ..
					  ```
			- Member function of base class are accessible in the derived class
				- ![image.png](../assets/image_1644736268264_0.png)
			- Visibility in derived class
				- ```cpp
				  class A : public B{...}
				  // All methods in B have same visibility in A
				  ```
				- ```cpp
				  class A : protected B{...}
				  // public and protected members of B become protected in A
				  ```
				- ```cpp
				  class A : private B{...}
				  // All members of base B become private in derived class A
				  ```
	- ## Initialization & Finalization
		- Most languages provide some sort of mechanism to initialize an object automatically at the beginning of its lifetime.
			- when written in form of a subroutine it is known as a constructor.
			- A constructor does not allocate space
		- Few languages provide destructor to finalize the object at the end of its lifetime.
		- ### Issues
			- Choosing a constructor
				- classes can have 0 or more constructors.
				- Distinguishing between them
				- C++, java, C#, Eiffel, SmallTalk - can have more than one constructor
				- Smalltalk, Eiffel
					- different constructors have different names
					- code that creates object must name a constructor explicitly
			- References and values
				- If variables are references, then every object must be created explicitly
				- If variables are values, then object creation can happen implicitly as a result of elaboration
				- Java - variables refer to objects
				- C++ - allow a variable to have a value that is an object
			- Execution order
				- In case of derived class in C++, the constructors of base class is executed before the constructors of the derived class.
			- Garbage collection
				- If language has garbage collection then need for destructors is reduced.
- # Scripting Languages
	- Conventional languages focusses on efficiency, maintainability, portability and static detection of errors
		- Their type system is built around H/W level concepts
	- Languages that focus on flexibility, rapid development, local customisation and dynamic checking
		- Their type systems are built around tables, patterns, lists and files.
	- Modern scripting languages have two sets of ancestors
		- Command interpreters or shells of traditional batch or terminal computing
			- Eg: IBM's JCL, MS-DOS, Unix sh
		- Tools for text processing and report generation
			- Eg: IBMs RPG, Unix's sed and awk
	- ## Common Characteristics of Scripting Languages
	  collapsed:: true
		- Both batch and interactive use
			- Some languages like to read the entire code
			- Others will take instruction line by line or interpret code line by line
		- Economy of Expression
			- Scripting languages tend to have minimum boilerplate when writing code
			- Some make heavy use of punctuation and have very short identifiers(perl)
		- Lack of declarations, simple scoping rules
			- No declarations are required.
			- In perl, everything is global by default
			- In PHP, everything is local by default
			- In python, a variable is local to the block where it is assigned a value
		- Flexible typing
			- Due to lack of declaration, scripting languages are dynamically typed
		- Easy access to system facilities
			- Perl provides over 100 built in commands for OS functions
			- They are easier to use than the corresponding functions in C
		- Sophisticated pattern matching and string handling
			- They have rich facilities for pattern matching, searching, string manipulation
		- High level data types
			- Built in high level data types
			- Sets, bags, dictionaries, lists, tuple
	- ## Names and Scopes
	  collapsed:: true
		- Most scripting languages don't require variables to be declared
		- Few languages like perl and js permit optional declarations
			- Perl can run in a mode that requires declarations called use strict "vars"
		- Most scripting languages are dynamically typed
		- Nesting & Scoping conventions
			- Nested subroutines with static scoping - scheme, python, js, R
			- Subroutines with dynamic scoping - Tcl
			- Nested named subroutines - perl
			- First class anonymous local subroutines - Perl, Ruby, Scheme, Python, JS, R
		- ### Scope of undeclared variable
			- In languages with static scope without variable declarations
			- If we access a variable 'x', is x local, global or intermediary?
			- x will be used in a statement without it being declared. So how do you define whether it is local, global or etc.
			- In perl, variables are global unless explicitly declared
			- In php, variables are local unless
			- Ruby uses prefix characters
				- foo - local
				- $foo - global
				- @foo - instance varaible
		- ### Scope in Tcl
			- Employs dynamic scoping
			- Variables are not accessed automatically
				- They must be explicitly asked by programmers
			- "upvar" and "uplevel" commands are used for this
			- upvar
				- access a variable in a specified frame and gives it a new name
			- uplevel
				- provides a nested Tcl script
				- This script is executed in the context of specified frame using call by name mode
		- ### Scoping in Perl
			- Global by default
			- local operator to declare variables that are dynamically scoped
			- my operator to declare variables that are statically scoped
	- ## Data Types
	  collapsed:: true
		- No declaration of variables in scripting languages
			- So it performs run time checks to ensure if values are used properly
		- Scheme, Python, Ruby performs strict checking
			- So explicit conversion needed for one type to another.
		- Perl, rexx, tcl will not show error
			- ```perl
			  $a[3] = "1";
			  print $a[3] + $a[4]; # $[4] is undefined
			  # but undefined evaluates to 0
			  # => 1 + 0 = 1
			  # 1 is printed
			  ```
		- Perl uses value model for variables
		- Scheme, python, ruby use reference model of variables
		- PHP and javascript use value model for primitive type variables and reference model for variables of object type
	- ## Numeric Types
		- JS - Numbers are double precision FP
		- Tcl - Numbers are string
			- Converted to int or float as needed
		- PHP - supports integers and double precision FP
		- Perl, Ruby - Supports integers, double precision FP, arbitrary precision integers (long int)
		- Python supports bignums, complex number
		- Scheme - all above formats + rationals
		- Ruby - classes for fix num, big num, float
	- ## Composite Types
		- check ppt
	- Set operations in Python
		- check ppt
	- ## Regular Expression
		- ??
	- ## Object Orientation
		- Perl has OOP features
		- PHP and js have OOP and imperative features
		- Python and Ruby only have OOP