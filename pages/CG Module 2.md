public:: true

- Syllabus
	- Line Drawing Algorithm- DDA, Bresenham’s algorithm –
	- Circle Generation Algorithms –Mid point circle algorithm, Bresenham’s algorithm-
	- Scan Conversion-frame buffers – solid area scan conversion –
	- polygon filling algorithms
- ![Module 2 by Miss.pdf](../assets/Module_2_by_Miss_1637040594953_0.pdf)
- Graphics Output Primitives
	- Points
		- A point is shown by a illuminating pixel on screen
	- Lines
		- A line segment is defined in terms of its two endpoints
	- Line is produced by means of illuminating a set of intermediary pixels between the two endpoints
# Line Drawing Algorithms
	- Line drawing is done by calculating the intermediate positions along the line path between the specified end point positions
	- ## DDA
		- Advantages
			- It eliminates multiplication
			- Does not calculate coordinates based on the complete equation (uses offset method)
		- Disadvantages
			- Round offs are accumulated, thus line diverges more and more from straight line
			- Round off operations take time
	- ## DAA vs Bresenham's
		- |                       | DAA                                                              | Bresenham's                                    |
		  |-----------------------|------------------------------------------------------------------|------------------------------------------------|
		  | Arithmetic            | uses floating points i.e. real arithmetic                        | Uses fixed points i.e. integer arithmetics     |
		  | Operations            | uses multiplication and division                                 | uses subtraction and addition                  |
		  | Speed                 | relatively slow due to real arithmetic                           | Faster due to integer arithmetic               |
		  | Accuracy & Efficiency | relatively less accurate and efficient                           | More efficient and accurate                    |
		  | Round off             | round off coordinates to the integer that is nearest to the line | Does not round off but takes incremental value |
		  | Expensive             | More expensive due to real arithmetic                            | Less expensive due to integer arithmetic       |
- Polygon Filling Algorithms
	- Filling a polygon means highlighting all the points inside a polygon with any color other than the background color
	- Two approaches
		- start from a seed pixel and work your way outside
		- next is to check whether a pixel is inside the polygon or outside and then fill it. This approach is called scan line algorithm
	- Boundary Fill Algorithm
		- It is a seed fill approach
		- algorithm works only if the boundary color of the polygon is different from the region which has to be filled
		- If the boundary is of single color then this algorithm proceeds from a point inside the polygon till it hits the boundary
		- Working
			- take an interior point, a fill color and the boundary color as input
			- check the color of the point
			- if it is not fill color or boundary color then fill it with fill color then call the function on its neighbours
			- If it is fill color or boundary color then do not call its neighbours
			- this process continues until all points up to the boundary color have been tested
		- Neighbours
			- 4 connected pixel
			- 8 connected pixel
		- Boundary fill code for 4 connected
		  ```
		  void boundaryFill(int x, int y, int fillColor, int borderColor){
		      getPixel(x, y, color);
		      if((color != borderColor) && (color != fillColor)){
		          setPixel(x, y);
		          boundaryFill(x+1, y, fillColor, borderColor);
		          boundaryFill(x-1, y, fillColor, borderColor);
		          boundaryFill(x, y+1, fillColor, borderColor);
		          boundaryFill(x, y-1, fillColor, borderColor);
		      }
		  }
		  ```
		- 4 connected pixel method won't cover all the pixels
	- Flood Fill Algorithm
		- using seed point approach
		- uses either 4 connected or 8 connected approach
		- suitable for multi color boundaries
		- When boundary is of many colors and interior is to be filled with one color
		- Algorithm
		  ```
		  void floodFill(int x, int y, int fillColor, int oldColor){
		      if(getPixel(x, y) == oldColor){
		          setColor(fillColor);
		          setPixel(x, y);
		          floodFill(x+1, y, fillColor, oldColor);
		          floodFill(x-1, y, fillColor, oldColor);
		          floodFill(x, y+1, fillColor, oldColor);
		          floodFill(x, y-1, fillColor, oldColor);
		      }
		  }
		  ```
		-