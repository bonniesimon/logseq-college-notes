file-path:: ../assets/Module4_DMW_1653538450377_0.pdf
file:: [Module4_DMW_1653538450377_0.pdf](../assets/Module4_DMW_1653538450377_0.pdf)
title:: hls__Module4_DMW_1653538450377_0

- xtremely  slow,  they  are  highly accurate,  
  ls-type:: annotation
  hl-page:: 10
  id:: 62b158e0-77a9-4148-9bfa-fb277d7da983
- less prone to overfitting than other methods
  ls-type:: annotation
  hl-page:: 10
  id:: 62b158e9-4d9e-4000-b948-1561687067c7
- used for numeric prediction as well as classification.
  ls-type:: annotation
  hl-page:: 10
  id:: 62b158f3-6e0f-46b4-8f5d-2e85b4ca72fc
- handwritten  digit  recognition, object  recognition, and  speaker identification
  ls-type:: annotation
  hl-page:: 10
  id:: 62b158fc-e389-4fcb-ae1a-fc5ac324fd30