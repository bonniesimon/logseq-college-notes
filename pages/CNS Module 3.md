- ![Module 3 by Miss.pdf](../assets/Module_3_by_Miss_1639472171327_0.pdf)
- Syllabus
	- Public key Cryptography: - Principles of Public key Cryptography Systems, Number theory- Fundamental Theorem of arithmetic, Fermat’s Theorem, Euler’s Theorem, Euler’s Totient Function, Extended Euclid’s Algorithm, Modular arithmetic. RSA algorithm- Key Management - Diffie-Hellman Key Exchange, Elliptic curve cryptography
- ??
# Public Key Cryptographic Systems
	- Public key or asymmetric cryptography is an encryption scheme that uses two mathematically related, but not identical keys - a public key & a private key
	- Unlike symmetric key algorithms, where one key is used for encryption and decryption, here each key performs a unique function
	- Public key cryptography is based on mathematical functions rather than substitutions or permutations
	- Major problems with Symmetric key ciphers
		- Key distribution
		- No. of keys required in a network
		- Digital signatures
	- Public key encryption has 6 ingredients
		- plain text
		- encryption algorithm
		- Public & private keys (KU, KR)
		- Cipher text
		- Decryption algorithm
	- Applications of Public Key Cryptosystems
		- Encryption / Decryption
		- Digital signatures
		- Key exchange
	- Requirements for Public Key cryptography
		- ??
	- Digital signatures
		- They serves the purpose of authentication and verification of documents and files
		- Crucial to prevent tampering or digital manipulation or forgery
		- Typically for asymmetric key system, public key for encryption and private key for decryption. However for digital signatures, it is the opposite.
		- Private key for encryption and public key for decryption.
		- ?Authentication can be done if we use the above approach
		- There are two methods
			- RSA
			- DSA
# RSA
# Diffie-Hellman Key Exchange
	- Purpose of the algorithm is to enable two users to securely exchange a key that can be used for subsequent symmetric encryption of messages
	- This algorithm depends for its effectiveness on the difficulty of computing discrete logarithms
	- ??
	- Security of DH key Exchange
		- Brute force attack
		- Man in the middle attack
			- can be avoided by using digital signatures and public key certificates
# Elliptic Curve cryptography
	- Elliptic curve cryptography (ECC) is used to implement public key cryptography
	- Based on latest mathematics and delivers a relatively more secure foundation than the first generation public key cryptography, like RSA
	- ECC is faster
	- Uses smaller key
	- Provides same security