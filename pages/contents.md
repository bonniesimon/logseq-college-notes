public:: true

- What's **Contents**?
  collapsed:: true
	- It's a normal page called [[Contents]], you can use it for:
	- 1. table of content/index/MOC
	- 2. pinning/bookmarking favorites pages/blocks (e.g. [[Logseq]])
	- collapsed:: true
	  	  3. You can also put many different things, depending on your personal workflow.
## Previous Papers
collapsed:: true
	- [[DBMS]]
	- [[Operating System]]
- ## Semester 6
  collapsed:: true
	- [[CS302 Design and Analysis of Algorithms]]
	- [[CS306 Computer Networks]]
	- [[CS308 Software Engineering & Project Management]]
	- [[CS332 Microprocessor Lab]]
	- [[CS334 Network Programming Lab]]
	- [[CS364 Mobile Computing]]
	- [[CS368 Web Technologies]]
	- [[HS300 Principles of Management]]
	- [[S6 Exam prep]]
	- [[NPTEL Internet of Things]]
	- Solved [question papers](https://www.ktuassist.in/2019/04/ktu-s6-cse-solved-question-papers.html)
## Semester 7
collapsed:: true
	- [[CS401 Computer Graphics]]
	- [[CS403 Programming Paradigms]]
	- [[CS405 Computer System Architecture]]
	- [[CS407 Distributed Computing]]
	- [[CS409 Cryptography and Network Security]]
	- [[CS467 Machine Learning]]
	- [[CS431 Compiler Design Lab]]
	- [[CS463 Digital Image Processing]]
	- [[CS451 Seminar]]
	- [[S7 Exam Prep]]
## Semester 8
	- [[CS492 Project]]
	- [[CS468 Cloud Computing]]
	- [[MP469 Industrial Psychology & Organizational Behaviour]]
	- [[CS404 Embedded Systems]]
	- [[CS402 Data Mining & Warehousing]]
	- [[S8 Exam Prep]]
## Placement / Interview Prep
	- [[TnP]]
	- [[Interview Prep]]
	- [[Resume]]