- Disadvantage & Advantage
	- Other methods in the papers
		- performance in edge device
	- Between other clinical methods
		- time lagging, more time to diagnose
	- MRI & other methods
- SZ diagnosis via EEG signals has always been challenging
- [Comparing different methods](http://noiselab.ucsd.edu/ECE228_2019/Posters/Group14.pdf)
- KNN
	- Makes prediction based on similarity betweeen distributions
	- computes its k nearest neighbours under a feature metric for each smaple
	- KNN is a baseline model, it is uses each series neighbours to make predictions
		- Each prediction is made based on a small part of training set
		- highly vulnerable to noise in the dataset
- LSTM
	- RNN are networks with loops in them
	- LSTM networks are a special type of RNN with well-defined structures which enable them to bridge very long lags
	- Compared to KNN, logistic regression & random forest algorithms - CNN LSTM provide an approach to automatically exploit wave feature in the series and create more complex model.
	- deep learning-based algorithms such as LSTM outperform traditional-based algorithms such as ARIMA model. More speciﬁcally, the average reduction in error rates obtained by LSTM was between 84 - 87 percent when compared to ARIMA indicating the superiority of
	  LSTM to ARIMA.
	- Furthermore, it was noticed that the number
	  of training times, known as “epoch” in deep learning, had no
	  effect on the performance of the trained forecast model and it
	  exhibited a truly random behavior. [source](https://www.researchgate.net/publication/330477082_A_Comparison_of_ARIMA_and_LSTM_in_Forecasting_Time_Series)
	- More speciﬁcally, the
	  LSTM-based algorithm improved the prediction by 85% on
	  average compared to ARIMA.
- Why use CNN & LSTM instead of only LSTM
	- CNN leads to a variety of different complexity reductions by concentrating on the key features. The use of convolution layers leads to a reduction in the size of tensors. Also the use of pooling leads to a further reduction. And last but not least the ReLu layer reduces the complexity. Because of the training time decrease
	- [source](https://stackoverflow.com/questions/58910504/why-cnn-lstm-is-faster-than-lstm)
- Clinical Approaches
	- Treatment is much more effective if doctors can diagnose the condition early. Currently, however, specialists cannot diagnose schizophrenia until an individual has had their first psychotic episode. At this point, an individual’s behavior can change dramatically, and they “may lose touchTrusted Source with some aspects of reality.”
	- [source](https://www.medicalnewstoday.com/articles/323625)
- [Mapping relationships among schizophrenia, bipolar and schizoaffective disorders: A deep classification and clustering framework using fmri time series](https://www.frontiersin.org/articles/10.3389/fnhum.2011.00028/full)
	- Disadvantages
		- High false positive rates
- [Structural and diffusion MRI based schizophrenia classification using 2D  pretrained and 3D naive Convolutional Neural Networks](https://personal.ntu.edu.sg/ctguan/Publications/2021_Mengjiao_SchRes.pdf)
	- Disadvantages
		- High computational cost during training caused by the high dimensionality of input data and the large number of parameters may constrain the development of 3D CNN model
- [A review of diffusion tensor imaging studies in schizophrenia](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2768134/)
	- Disadvantage
		- Lacks the ability to conclude with certainty the nature of white matter pathology in schizophrenia.
- [EEG correlates of face recognition in patients with schizophrenia spectrum disorders: A systematic review ](https://pubmed.ncbi.nlm.nih.gov/31003117/)
	- Disadvantages
		- Older patients with longer disease duration show more consistent neuropsychological correlations
- [Development of a machine learning based algorithm to accurately detect schizophrenia based on one-minute EEG recording](https://scholarspace.manoa.hawaii.edu/bitstream/10125/64135/0317.pdf)