public:: true

- ![Module 2 by Miss.pdf](../assets/Module_2_by_Miss_1649767894328_0.pdf)
- Syllabus
	- System Models for Distributed and Cloud Computing –
	- Software Environments for Distributed Systems and Clouds –
	- Cloud Computing and Service Models – Public – Private – Hybrid Clouds – Infrastructure-as-a-Service (IaaS) – Platform-as-a- Service (PaaS) - Software-as-a-Service (SaaS)-
	- Different Service Providers
- # System Models for Distrubuted and Cloud computing
	- ??
- # Software Environments for Distributed systems and clouds
	- ## Service Oriented Architecture (SOA)
		- An architectural approach in which applications make use of services available in the network
		- An applications business logic and functions are modularized and presented as services to be used by consumers / clients
		- Loosely coupled in nature
			- The service interface is independent of the implementation
		- Developers can build applications by composing different services without knowing the service's implementation
		- Services can be implemented on different applications and the application consuming them can be on different platforms
		- To major roles in service oriented architecture
			- Service providers
			- Service consumers
	- ## Distributed OS
		- ??
	- ## Parallel & Distributed Programming Models
		- ### Message Passing Interface (MPI)
			- Programming model used to run concurrent and parallel programs in a distributed system
			- MPI is a library of subprograms that can be called
			- MPI's goals are high performance, scalability and portability
		- ### MapReduce
			- Web programming model for scalable data processing on large clusters over large data sets
			- Applied mainly in web scale search and cloud computing applications
			- The user specifies a Map function to generate a set of intermediate key value pairs
			- Then applies a Reduce function to merge all intermediate values with the same intermediate keys
			- MapReduce is highly scalable to explore high degrees of parallelism at different job levels
		- ### Hadoop
			- Developed by Yahoo
			- ??
		- ### Open Grid Services Architecture
			- ??
		- ### Globus Toolkits and Extension
			- ??
- # Cloud Computing Models
	- Cloud computing has evolved from cluster, grid and utility computing
	- Cluster and grid leverage the use of parallel computers to solve problems of any size
	- ## Public Clouds
		- It is built over the internet
		- Anyone can access the services once they pay for it
		- Eg: AWS, Azure, salesforce, etc
		- Providers offer a publicly accessible remote interface for creating and managing VM instances within their proprietary infrastructure
		- The application and infrastructure services are offered at flexible price per use basis
		- It delivers a selected set of business processes
		- Advantages of public cloud
			- Lower costs
				- no need to purchase hardware or software
			- No maintenance
				- providers deals with the maintenance
			- Near unlimited scalability
			- High reliability
	- ## Private Clouds
		- It is built within the domain of an intranet, owned by a single organization
		- Client owned and managed
			- access is limited to the owning clients and their partners
		- Not meant for sell capacity
		- It is supposed to deliver more efficient and convenient cloud services
		- Advantages
			- More flexibility
			- More control
			- More scalability
	- ## Hybrid Clouds
		- It is built with both public and private clouds
		- Private cloud can support hybrid cloud by supplementing local infrastructure with computing capacity from an external public cloud
		- Hybrid cloud allows access to clients, the partner networks and third parties
		- Advantages
			- Control
			- Flexibility
			- Cost effectiveness
			- Ease
- # Cloud Service Models
	- The services provided over the cloud are categorized as
		- IaaS
		- SaaS
		- PaaS
	- ## IaaS
		- It allows users to use virtualized IT resources for computing, storage and networking.
		- The service is performed by the rented cloud infrastructure
		- The user can deploy and run his application over his chosen OS environment
		- Users do not manage or control the underlying cloud infrastructure but has control over OS, storage, deployed application, etc
		- It encomposes
			- storage as a service, compute instances as a service and communication as a service
		- Key Features
			- Instead of purchasing hardware outright, users pay for IaaS on demand
			- Infrastructure is scalable depending on processing and storage
			- Saves enterprise cost of buying and maintaining their own hardware
			- Because data is on the cloud, there is not single point of failure
		- Eg: Amazon Virtual Private Cloud (VPC)
	- ## PaaS
		- This model provides users with a cloud environment in which they can develop, manage and deliver applications
		- Platform includes OS and runtime library support
		- An integrated computer system  consisting of hardware and software infrastructure
		- In addition to storage and computing resources, users are able to use prebuilt tools available on the platform
		- User does not manage the underlying cloud infrastructure
		- Key Features
			- PaaS provides a platform with tools to test, develop and host applications in the same environment
			- Enables organizations to focus on the development without having to worrying about the underlying infrastructure
			- Providers manage security, OS, server software and backups
			- Facilitates collaborative work
	- ## SaaS
		- This model provides software applications as a service
		- Provides users with access to a vendor's cloud based software
		- Users don't have to install applications on their device
		- Instead the applications reside on the cloud
		- Key Features
			- SaaS vendors provide cloud applications to users via a subscription model
			- Users do not have to manage install or upgrade software
			- Data is secure in the cloud
			- Use of resources can be scaled depending on the service needs
			- Applications are accessible from almost any internet connected device from virtually anywhere in the world