- ![Module 6 by Miss.pdf](../assets/Module_6_by_Miss_1643195193539_0.pdf)
- Questions
	- What is a thread pool in java? What is its use?(4)
	- What is busy waiting? What is its principal alternative?(4)
	- What do you mean by late binding of machine code? What are its advantages
	  and disadvantages?(6)
	- Write short notes on Virtual Machines.(3)
	- What is quasi parallelism?(3)
	- Explain busy wait synchronization.(6)
	- What is a thread pool in Java? What purpose does it serve?(4)
	- In what sense is fork/join more powerful than co-begin?(4)
	- What is a semaphore? What operations does it support? How binary and general
	  semaphore does differ?(6)
	- Describe six different mechanisms(principles) commonly used to create new
	  threads of control in a concurrent program(9)
	- What is a JIT compiler? What are its potential advantages over
	  interpretation/conventional compilation? (3)
	- Remaining 2019 may, 2019 dec
- Syllabus
	- Concurrency:- Threads, Synchronization. Run-time program Management:- Virtual Machines, Late Binding of Machine Code, Reflection, Symbolic Debugging, Performance Analysis.
- A program is concurrent if it has more than one active execution context
- A system is concurrent if two or more tasks are executed at same time.
- If threads are executing in different processes it is said to have parallelism
- # Communication and Synchronization
	- Communication refers to any mechanism that allows one thread to obtain information produced by another
	- Two mechanisms
		- Message Passing
			- A thread can communicate with another by explicit send operation
		- Shared memory
			- A thread can communicate with others by writing to a common variable
	- Synchronization is any mechanism than allows the programmer to control the relative order in which operations occur in different threads
	- Two mechanisms
		- Spinning (busy waiting)
			- Threads runs a loop until some condition becomes true
			- eg: until a message queue becomes empty
		- Blocking
			- In blocking synchronization, the waiting thread voluntarily relinquishes it precessor to some other thread
			- A thread that makes the condition true in the future will take action to make the blocked thread run again
- ## Languages and Libraries
	- Thread level concurrency can be provided by
		- explicit concurrent languages
		- compiler supported extensions
		- library packages
	- All parallel programming use libraries for synchronisation or message passing
	- UNIX
		- Shared memory parallelism using POSIX pthread standards
	- Windows
		- Win32 thread package
	- Language support for concurrency was provided in Algol 68 and Ada
	- Fortran has parallel execution of loops using OpenMP
	- Concurrent languages is more advantageous than library packages in compiler support
- # Thread Creations
	- All concurrent systems allows threads to be created dynamically
	- ## Co-begin
		- Normally a compound statement call for sequential execution of constituent statements
			- But a co-begin construct calls instead for concurrent execution
		- All statements run concurrently
		- ```algo-68
		  co-begin
		  	stmnt1
		      stmnt2
		      ....
		      stmnt3
		  end
		  ```
		- Co-begin is the principal means of creating threads in Algol-68
		- This appears in other systems, such as OpenMP
		- ```c
		  #pragma omp sections 
		  { 
		    # pragma omp section 
		    { 
		      printf("thread 1 here\n"); 
		    }
		  	# pragma omp section 
		    { 
		      printf("thread 2 here\n"); 
		    }
		  }
		  ```
	- ## Parallel Loops
		- Loops whose iterations are to be executed concurrently
	- ## Launch at Elaboration
		- Declare a thread as a procedure without parameters. When declaration is elaborated, the thread is created to execute the code.
		- In Ada threads are called Tasks(T)
	- ## Fork / Join
		- Co-begin, parallel loops and launch at elaboration all lead to a concurrent control flow pattern in which thread executions are properly nested.
		- The fork makes the creation of threads an explicit, executable operation
		- Join allows a thread to wait for the completion of a previously forked thread.
	- ## Implicit Receipt
		- ??
	- ## Early Repy
		- ??
- # Implementation of Threads
	- threads of a concurrent program are written on top of processes provided by the OS
	- We can either use
		- a separate OS process for every thread or
		- multiplex all of a program's threads on top of a single process
	- ??
	- Threads via coroutines
		- Implemented using coroutines
		- Done using 3 steps
			- First the argument to transfer is hidden by implementing a scheduler which chooses the next thread to run when the current thread yields the processor
			- Then we implement a preemption which suspends the current thread and gives chance to other threads to run
			- We allow the data structures that describe our collection of threads to be shared by more than one OS process, so that the threads can run on any of the processes.
	- ??
- # Synchronization
	- Controls the order in which operations occur in different threads
	- Used to eliminate race around conditions
	- Done by two mechanisms
		- Blocking
		- Busy waiting or spinning
	- Synchronization can be
		- Mutual Exclusion
			- Making sure that only one process is executing a critical section at a time
		- Condition Synchronization
			- Making sure a process does not proceed until some condition holds
	- ## Busy - Wait Synchronization
		- Technique in which a process repeatedly checks to see if a condition is true
		- ### Spin locks
			- This is a lock that cause the thread trying to acquire it to wait in a loop while repeatedly checking if the lock is available
			- Test and set instruction writes 1 to the memory location and returns its old value as a single atomic
			- Calling process acquires the lock if the old value was 0 otherwise it busy waits
			- Disadvantage
				- When p1 has the lock, and p2 is waiting for the lock, p2 will keep on incurring the bus transactions in attempts to acquire the lock
				- This increases the bus traffic requirement, slows down all the other traffic
			- Test and test and set
				- It spins with ordinary reads until it appears that the lock is free
			- Backoff
				- When a thread tries to acquire the lock and fails, it waits sometime before retrying.
		- ### Barriers
			- Correctness of depends on making sure that every thread completes the previous step before any moves on to the next. A Barriers serves to provide this synchronization
			- Implemented using a shared counter modified by an atomic fetch and decrement instruction
			- Counter starts at n
			- it is decremented as each thread reaches a barrier
			- When counter becomes 0, it allows other threads to proceed.
	- ## Non Blocking Algorithms
		- ??
	- ## Scheduler based synchronization
		- The problem with scheduler based synchronization is that it consumes processor cycles.
		- Busy wait synchornization makes sense only if
			- one has nothing better to do with the current processor
			- or the expected wait time is less than the time context switch to some other thread and switch back
	- ## Semaphore
		- Semaphores are the oldest of the scheduler based synchronization mechanisms
		- Semaphore is basically a counter with two associated operations, P and V
		- Thread that calls P atomically decrements the counter then waits until it is non negative
		- A thread that calls V atomically increments the counter and wakes up a thread, if any
		- P acquires the lock, V releases it
- # Runtime Program Management
	- Late binding of machine code
		- Compiler produces a target code in machine language that can be executed several times for different inputs
		- In some environments the translation and execution are combined.
		- Many systems delay the binding of a program to its machine code
			- Just in time compilers
	- ## Just in Time (JIT) Compiler
		- It is an essential part of JRE (Java Runtime Environment)
		- responsible for performance optimization
		- used to increase efficiency of interpreter
		- Compiles programs immediately prior to execution
	- ## Dynamic Compilation
		- In some cases JIT compilation must be delayed